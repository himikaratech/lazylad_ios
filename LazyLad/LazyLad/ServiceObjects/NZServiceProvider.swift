//
//  NZServiceProvider.swift
//  LazyLad
//
//  Created by Lazylad on 6/9/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class NZServiceProvider: NSObject, NSCoding {
   
    var sp_id:Int!;
    var sp_code:String!;
    var sp_name:String!;
    var sp_number:String!;
    var sp_type:String!;
    var sp_del_time:String!;
    var sp_min_order:String!;
    
    
    
    // Init Method For Service Provider
    init(spID:Int, spCode:String, spName:String, spNumber:String, spType:String, spDel:String, spMinOrder:String) {
        sp_id = spID;
        sp_code = spCode;
        sp_name = spName;
        sp_number = spNumber;
        sp_type = spType;
        sp_del_time = spDel;
        sp_min_order = spMinOrder;
    }
    
    required init(coder decoder: NSCoder) {
        self.sp_id = decoder.decodeObjectForKey("id") as! Int?
        self.sp_code = decoder.decodeObjectForKey("code") as! String?
        self.sp_name = decoder.decodeObjectForKey("name")as! String?
        self.sp_number = decoder.decodeObjectForKey("number")as! String?
        self.sp_type = decoder.decodeObjectForKey("type")as! String?
        self.sp_del_time = decoder.decodeObjectForKey("deltime")as! String?
        self.sp_min_order = decoder.decodeObjectForKey("minorder")as! String?
        
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.sp_id, forKey: "id");
        coder.encodeObject(self.sp_code, forKey: "code");
        coder.encodeObject(self.sp_name, forKey: "name");
        coder.encodeObject(self.sp_number, forKey: "number");
        coder.encodeObject(self.sp_type, forKey: "type");
        coder.encodeObject(self.sp_del_time, forKey: "deltime");
        coder.encodeObject(self.sp_min_order, forKey: "minorder");
       
    }

}
