//
//  NZAddNewAddress.swift
//  LazyLad
//
//  Created by Lazylad on 6/10/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class NZAddNewAddress: NSObject {

    var ad_user_code:Int!;
    var ad_user_address_code:Int!;
   
    
    // Init Method For Service Category
    init(adUserCode:Int,adUserAdrsCode:Int) {
        ad_user_code   = adUserCode;
        ad_user_address_code = adUserAdrsCode;
        
    }
    
    required init(coder decoder: NSCoder) {
        self.ad_user_code = decoder.decodeObjectForKey("user_code") as! Int?
        self.ad_user_address_code = decoder.decodeObjectForKey("user_address_code") as! Int?
       
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.ad_user_code, forKey: "user_code");
        coder.encodeObject(self.ad_user_address_code, forKey: "user_address_code");
            }
    
}
