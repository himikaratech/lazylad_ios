//
//  NZCities.swift
//  LazyLad
//
//  Created by Lazylad on 6/9/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class NZCities: NSObject, NSCoding {
    var city_id:String!;
    var city_code:String!;
    var city_name:String!;
    
    
    
    // Init Method For Address
    init(idd:String, code:String, name:String) {
        city_id = idd;
        city_code = code;
        city_name = name;
    }
    
    
    required init(coder decoder: NSCoder) {
        self.city_id = decoder.decodeObjectForKey("id") as! String?
        self.city_name = decoder.decodeObjectForKey("name") as! String?
        self.city_code = decoder.decodeObjectForKey("code")as! String?
       
        
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.city_id, forKey: "id");
        coder.encodeObject(self.city_name, forKey: "name");
        coder.encodeObject(self.city_code, forKey: "code");
        
    }

}
