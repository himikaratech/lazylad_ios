//
//  NZItemsServiceProviderCategoryWise.swift
//  LazyLad
//
//  Created by Lazylad on 6/10/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class NZItemsServiceProviderCategoryWise: NSObject {
    var is_id: Int!
    var is_item_code: String!
    var is_sp_code: String!
    var is_item_type_code: String!
    var is_item_name: String!
    var is_item_unit: String!
    var is_item_quantity: String!
    var is_item_cost: Int!
    var is_item_img_flag: Int!
    var is_item_img_address: String!
    var is_item_short_desc: String!
    var is_item_desc: String!
    var is_item_status: Int!
    var is_item_selected: Int!
    var is_item_quantity_selected: Int!
    var is_item_service_type: String!
    var is_item_service_category: String!
    var is_item_mrp: Int!

    
    // Init Method For Service Category
    init(isID:Int,isItemCode:String,isSpCode:String,
     isItemTypeCode: String,
     isItemName: String,
     isItemUnit: String,
     isItemQuantity: String,
     isItemCost: Int,
     isItemImg_flag: Int,
     isItemImg_address: String,
     isItemShort_desc: String,
     isItemDesc: String,
     isItemStatus: Int,
     isItemSelected: Int,
     isItemQuantity_selected: Int,
     isItemService_type: String,
     isItemService_category: String,
     isItemMRP: Int
) {
    
     is_id = isID
     is_item_code = isItemCode
     is_sp_code = isSpCode
     is_item_type_code = isItemTypeCode
     is_item_name = isItemName
     is_item_unit = isItemUnit
     is_item_quantity = isItemQuantity
     is_item_cost = isItemCost
     is_item_img_flag = isItemImg_flag
     is_item_img_address = isItemImg_address
     is_item_short_desc = isItemShort_desc
     is_item_desc = isItemDesc
     is_item_status = isItemStatus
     is_item_selected = isItemSelected
     is_item_quantity_selected = isItemQuantity_selected
     is_item_service_type = isItemService_type
     is_item_service_category = isItemService_category
     is_item_mrp = isItemMRP
    }

    
 required init(coder decoder: NSCoder) {
    self.is_id = decoder.decodeObjectForKey("id") as! Int?
    self.is_item_code = decoder.decodeObjectForKey("itemCode") as! String?
    self.is_sp_code = decoder.decodeObjectForKey("spCode") as! String?
    self.is_item_type_code = decoder.decodeObjectForKey("itemTypeCode") as! String?
    self.is_item_name = decoder.decodeObjectForKey("itemName") as! String?
    self.is_item_unit = decoder.decodeObjectForKey("itemUnit") as! String?
    self.is_item_quantity = decoder.decodeObjectForKey("itemQuantity") as! String?
    self.is_item_cost = decoder.decodeObjectForKey("itemCost") as! Int?
    self.is_item_img_flag = decoder.decodeObjectForKey("itemImgFlag") as! Int?
    self.is_item_img_address = decoder.decodeObjectForKey("itemImgAddress") as! String?
    self.is_item_short_desc = decoder.decodeObjectForKey("itemSortDesc") as! String?
    self.is_item_desc = decoder.decodeObjectForKey("itemDesc") as! String?
    self.is_item_status = decoder.decodeObjectForKey("itemStatus") as! Int?
    self.is_item_selected = decoder.decodeObjectForKey("itemSelected") as! Int?
    self.is_item_quantity_selected = decoder.decodeObjectForKey("quantitySelected") as! Int?
    self.is_item_service_type = decoder.decodeObjectForKey("itemServiceType") as! String?
    self.is_item_service_category = decoder.decodeObjectForKey("itemServiceCategory") as! String?
    self.is_item_mrp = decoder.decodeObjectForKey("itemMRP") as! Int?
    
    }

    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.is_id, forKey: "id");
        coder.encodeObject(self.is_item_code, forKey: "itemCode");
        coder.encodeObject(self.is_sp_code, forKey: "spCode");
        coder.encodeObject(self.is_item_type_code, forKey: "itemTypeCode");
        coder.encodeObject(self.is_item_name, forKey: "itemName");
        coder.encodeObject(self.is_item_unit, forKey: "itemUnit");
        coder.encodeObject(self.is_item_quantity, forKey: "itemQuantity");
        coder.encodeObject(self.is_item_cost, forKey: "itemCost");
        coder.encodeObject(self.is_item_img_flag, forKey: "itemImgFlag");
        coder.encodeObject(self.is_item_img_address, forKey: "itemImgAddress");
        coder.encodeObject(self.is_item_short_desc, forKey: "itemSortDesc");
        coder.encodeObject(self.is_item_desc, forKey: "itemDesc");
        coder.encodeObject(self.is_item_status, forKey: "itemStatus");
        coder.encodeObject(self.is_item_selected, forKey: "itemSelected");
        coder.encodeObject(self.is_item_quantity_selected, forKey: "quantitySelected");
        coder.encodeObject(self.is_item_service_type, forKey: "itemServiceType");
        coder.encodeObject(self.is_item_service_category, forKey: "itemServiceCategory");
        coder.encodeObject(self.is_item_mrp, forKey: "itemMRP");

 }

}
