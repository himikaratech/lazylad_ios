//
//  NZPreviousOrder.swift
//  LazyLad
//
//  Created by Lazylad on 6/11/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class NZPreviousOrder: NSObject {
    var order_code:String!;
    var order_date:String!;
    var order_amount:Int!;
    var order_status:String!;
    var sp_name:String!;
    
    
    // Init Method For Service Category
    init(odrCode:String, odrDate:String, odrAmount:Int, odrStatus:String, spName:String) {
        order_code = odrCode;
        order_date = odrDate;
        order_amount = odrAmount;
        order_status = odrStatus;
        sp_name = spName;
    }
    
    required init(coder decoder: NSCoder) {
        self.order_code = decoder.decodeObjectForKey("odrCode") as! String?
        self.order_date = decoder.decodeObjectForKey("odrDate") as! String?
        self.order_amount = decoder.decodeObjectForKey("odrAmount")as! Int?
        self.order_status = decoder.decodeObjectForKey("odrStatus")as! String?
        self.sp_name = decoder.decodeObjectForKey("spName")as! String?

    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.order_code, forKey: "odrCode");
        coder.encodeObject(self.order_date, forKey: "odrDate");
        coder.encodeObject(self.order_amount, forKey: "odrAmount");
        coder.encodeObject(self.order_status, forKey: "odrStatus");
        coder.encodeObject(self.sp_name, forKey: "spName");

    }
    
}
