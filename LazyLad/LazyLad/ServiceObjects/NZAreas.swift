//
//  NZAreas.swift
//  LazyLad
//
//  Created by Lazylad on 6/9/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class NZAreas: NSObject, NSCoding {
    var area_id:String!;
    var area_name:String!;
    var area_code:String!;
    
    var city_code:String!;

    
    
    // Init Method For Address
    init(areaId:String, code:String, name:String, cityId:String) {
        area_id = areaId;
        area_code = code;
        area_name = name;
        city_code = cityId
    }
    
    
    required init(coder decoder: NSCoder) {
        self.area_id = decoder.decodeObjectForKey("arId") as! String?
        self.area_name = decoder.decodeObjectForKey("name") as! String?
        self.area_code = decoder.decodeObjectForKey("areacode")as! String?
        self.city_code = decoder.decodeObjectForKey("citycode") as! String?
        
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.area_id, forKey: "arId");
        coder.encodeObject(self.area_name, forKey: "name");
        coder.encodeObject(self.area_code, forKey: "areacode");
        coder.encodeObject(self.city_code, forKey: "citycode");
        
    }
    
}
