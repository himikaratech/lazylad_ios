//
//  NZServiceCategory.swift
//  LazyLad
//
//  Created by Lazylad on 6/10/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class NZServiceCategory: NSObject {
    var sc_id:Int!;
    var st_code:String!;
    var sc_code:String!;
    var sc_name:String!;
    
    // Init Method For Service Category
    init(scID:Int, stCode:String, scCode:String, scName:String) {
        sc_id   = scID;
        st_code = stCode;
        sc_code = scCode;
        sc_name = scName;
       
    }
    
    required init(coder decoder: NSCoder) {
        self.sc_id = decoder.decodeObjectForKey("id") as! Int?
        self.st_code = decoder.decodeObjectForKey("stcode") as! String?
        self.sc_code = decoder.decodeObjectForKey("sccode")as! String?
        self.sc_name = decoder.decodeObjectForKey("name")as! String?
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.sc_id, forKey: "id");
        coder.encodeObject(self.st_code, forKey: "stcode");
        coder.encodeObject(self.sc_code, forKey: "sccode");
        coder.encodeObject(self.sc_name, forKey: "name");
    }
 
}
