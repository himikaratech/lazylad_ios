//
//  NZBillOrderItems.swift
//  LazyLad
//
//  Created by Lazylad on 6/18/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class NZBillOrderItems: NSObject {
    var order_item_code:String!;
    var order_item_name:String!;
    var ordr_short_desc:String!;
    var order_item_quantity:String!;
    var ordr_item_cost:Int!;
    var ordr_item_description:String!;
    
    
    // Init Method For Service Category
    init(odrCode:String, ordrname:String,desc:String,quantity:String,cost:Int,description:String) {
        order_item_code = odrCode;
        order_item_name = ordrname;
        ordr_short_desc = desc;
        order_item_quantity = quantity;
        ordr_item_cost = cost;
        ordr_item_description = description;
    }
    
    required init(coder decoder: NSCoder) {
        self.order_item_code = decoder.decodeObjectForKey("itemcode") as! String?
        self.order_item_name = decoder.decodeObjectForKey("itemname") as! String?
        self.ordr_short_desc = decoder.decodeObjectForKey("desc")as! String?
        self.order_item_quantity = decoder.decodeObjectForKey("quantity")as! String?
        self.ordr_item_cost = decoder.decodeObjectForKey("itemcost")as! Int?
        self.ordr_item_description = decoder.decodeObjectForKey("description")as! String?
        
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.order_item_code, forKey: "itemcode");
        coder.encodeObject(self.order_item_name, forKey: "itemname");
        coder.encodeObject(self.ordr_short_desc, forKey: "desc");
        coder.encodeObject(self.order_item_quantity, forKey: "quantity");
        coder.encodeObject(self.ordr_item_cost, forKey: "itemcost");
        coder.encodeObject(self.ordr_item_description, forKey: "description");
        
    }

}
