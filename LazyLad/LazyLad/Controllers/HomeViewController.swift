//
//  HomeViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/3/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var btnEditAddress: UIButton!
    @IBOutlet var btnSlideMenu: UIButton!
    @IBOutlet var vwSlide: UIView!
    @IBOutlet weak var homeCollection: UICollectionView!
    @IBOutlet var customCell: HomeCustomCell!
    var addressView:EditAddressViewController!;
    var menuView:MenuViewController!;
    var  cellImagesArray:NSArray!;
    var  cellTextArray:NSArray!;
       
    var  cntller:UIViewController!;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblAddress.text = "\(Helper.fetchString(SECTORNAME)) \(Helper.fetchString(STATENAME))";
         NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("Notify"), name: NOTIFYFORCURRENTLOC, object: nil);
        self.btnSlideMenu.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
        addMenuView();
        addImages();
        gestuer();
        self.homeCollection .registerNib(UINib(nibName: "HomeCustomCell", bundle: nil), forCellWithReuseIdentifier: "cell");
         self.homeCollection.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
        
    }
    
    
    
    
    // Gesture Implement Here
    func gestuer(){
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    //Notificatiopn Fired
    func Notify(){
        self.lblAddress.text = "\(Helper.fetchString(SECTORNAME)) \(Helper.fetchString(STATENAME))";
    }
    
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                self.navMenuShow();
            case UISwipeGestureRecognizerDirection.Left:
                self.navMenuHide();
            default:
                break
            }
        }
    }
    
    // Add Images into Array
    func addImages(){
        cellImagesArray=NSArray(arrayLiteral: "grocery-img", "flower-img", "backery-img", "stetionary-img","meat-img", "fruits&veg-img");
        cellTextArray=NSArray(arrayLiteral:  "Groceries & Provisions", "Flowers", "Bakery", "Stationary", "Meats & Seafoods", "Fruits & Vegetables");
    }
    
    // Add Menu View As a Subview
    func addMenuView(){
        menuView = MenuViewController(title: "Home", controller: self);
        self.view.addSubview(menuView.view);
        menuView.view.frame = CGRectMake(-320, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func slideMenuBtnAction(sender: UIButton) {
        sender.selected = !sender.selected;
        self.navMenuShow()
//        if(!sender.selected){
//           self.navMenuHide()
//        }
//        else{
//            self.navMenuShow()
//        }
    }
    
    func navMenuHide()
    {
        UIView.animateWithDuration(0.5, animations: {
             self.menuView.view.frame = CGRectMake(-320, 0, self.menuView.view.frame.size.width, self.menuView.view.frame.size.height);
             self.btnSlideMenu.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
             })
    }
    func navMenuShow()
    {
        UIView.animateWithDuration(0.5, animations: {
            self.menuView.view.frame = CGRectMake(0, 0, self.menuView.view.frame.size.width, self.menuView.view.frame.size.height);
            self.btnSlideMenu.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
            
        })
        
    }
    
    //***** CollectionView Delegates *****//
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    //2
     func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellImagesArray.count;
    }
    
    //3
     func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! HomeCustomCell
        cell.lblText.text=cellTextArray.objectAtIndex(indexPath.row) as? String;
        if(indexPath.row == 1){
        cell.imgIcon.sd_setImageWithURL(NSURL(string: "\(HOME_IMAGE_URL_FLOWER_FULL)"), placeholderImage: UIImage(named: ""));
        }
        else{
         cell.imgIcon.sd_setImageWithURL(NSURL(string: "\(HOME_IMAGE_URL_FULLIMAGES)\(indexPath.row+1)"), placeholderImage: UIImage(named: ""));
        }
        //cell.imgIcon.image=UIImage(named: cellImagesArray.objectAtIndex(indexPath.row) as! String);

        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        println("Cell \(indexPath.row) selected")
        
        var imageSend: String = cellImagesArray[indexPath.row] as! String
        let data = imageSend.dataUsingEncoding(NSUTF8StringEncoding)
        //let controller = StoreViewController(data: data!, controller: self);
        if(indexPath.row == 1){
            let controller = StoreViewController(imgName: imageSend, imgNo:6, controller: self);
            self.navigationController!.pushViewController(controller, animated: true)
        }
        else{
            let controller = StoreViewController(imgName: imageSend, imgNo: indexPath.row, controller: self);
            self.navigationController!.pushViewController(controller, animated: true)
        }
    }
  
    @IBAction func editAddressBtnAction(sender: AnyObject) {
        addressView =  EditAddressViewController(isNavigateFromSplash: false, controller: self);
        self.view.addSubview(addressView.view);

    }
    
}
