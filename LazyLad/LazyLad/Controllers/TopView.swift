//
//  TopView.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.

import UIKit
protocol topDelegate {
    func clickedRight(); // this function the first controllers
}
class TopView: UIViewController {

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //Custom Delegate
    var delegate: topDelegate?
    

    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    var titleName:String = "";
    var cont:UIViewController;
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitle.text=self.titleName;
        // Do any additional setup after loading the view.
    }
    
    //===================================================================================================
    //INIT FUNCTION WITH PARAMETERS
    //===================================================================================================

    
   init(title:String,controller:UIViewController) {
        self.titleName=title;
        self.cont=controller;
        super.init(nibName: "TopView", bundle: nil);
    }
    
    @IBAction func clickToRight(sender: AnyObject) {
       // delegate?.clickedRight();
        println("Setting Cliked");
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func clickToBAck(sender: AnyObject) {
    }

    @IBOutlet weak var lblTitle: UILabel!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
