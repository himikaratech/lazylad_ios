//
//  EditAddressTableViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/8/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

protocol ListViewDelegate: class{
    func selectedData(value:String, viewNo:Int, id:String);
}

class EditAddressTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Make Enum For Selected Type Data
    
    enum TypeView : String {
        case FirstView = "FirstView"
        case SecondView = "SecondView"
        case Bursa = "Bursa"
    }

    
    @IBOutlet weak var tblList: UITableView!
    var listArray:NSMutableArray!;
    var viewController:UIViewController!;
    var delegate:ListViewDelegate!;
    var selectedView:Int!;
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    init(listDataArray:NSMutableArray,controller:UIViewController, selectedViewNo:Int) {
        listArray = listDataArray;
        viewController = controller;
        selectedView = selectedViewNo;
        super.init(nibName: "EditAddressTableViewController", bundle: nil);
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //***** TableView Delegates *****//
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return listArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("CELL") as? UITableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "CELL")
        }
        
        if(selectedView == 2){
            var areaObj:NZAreas = (listArray.objectAtIndex(indexPath.row) as? NZAreas)!;
            cell?.textLabel?.text = areaObj.area_name;
        }else{
        var citiObj:NZCities =  (listArray.objectAtIndex(indexPath.row) as? NZCities)!;
        cell?.textLabel?.text = citiObj.city_name;
        }
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if(selectedView == 2){
            var areaObj:NZAreas = (listArray.objectAtIndex(indexPath.row) as? NZAreas)!;
            Helper.saveStringInDefault(CURRENT_AREA, value: areaObj.area_name);
            self.delegate.selectedData(areaObj.area_name as String, viewNo: selectedView, id: areaObj.area_code as String)
        }
        else{
            var citiObj:NZCities =  (listArray.objectAtIndex(indexPath.row) as? NZCities)!;
            Helper.saveStringInDefault(CURRENT_CITY, value: citiObj.city_name);
            self.delegate.selectedData(citiObj.city_name as String, viewNo: selectedView, id: citiObj.city_id as String)
        }
    }

}
