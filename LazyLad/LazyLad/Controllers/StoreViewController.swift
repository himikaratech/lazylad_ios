//
//  StoreViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class StoreViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet var lblSellersNotFound: UILabel!
    @IBOutlet weak var tblStore: UITableView!
    @IBOutlet var selectedImage: UIImageView!
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var storeCustomCell: StoreViewCell!
    var contro:UIViewController!
    var imageData:NSData!
    var imggName:String!
    var stCode:Int!;
    var serviceProviderArray:NSMutableArray!;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceProviderArray = NSMutableArray();
        if(IJReachability.isConnectedToNetwork()){
            callServiceProviderApi();
        }
        else{
            Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
        }
        
       
        
        // Do any additional setup after loading the view.
    }
    
    // ********************* Service Provider API ************************
    func callServiceProviderApi(){
        MBProgressHUD.showHUDAddedTo(self.view, animated: true);
        var areaCode = Helper.fetchString(AREA_CODE) as! String;
        let manager = AFHTTPRequestOperationManager()
        manager.GET(
            "\(BASEURL)serviceProvidersForServiceTypeGSON/\(stCode)/\(areaCode)",
            parameters: nil,
            success: { (operation: AFHTTPRequestOperation!,
                responseObject: AnyObject!) in
                println("JSON: " + responseObject.description)
                
                let results: NSArray = (responseObject["service_providers"] as? NSArray)!
                
                var dict:NSDictionary = NSDictionary();
                
                for (var i=0; i<results.count;++i){
                    dict = results[i] as! NSDictionary;
                    var serviceId = dict.valueForKey("id") as! Int;
                    var serviceName = dict.valueForKey("sp_name") as! String;
                    var serviceCode = dict.valueForKey("sp_code") as! String;
                    var serviceDelTime = dict.valueForKey("sp_del_time") as! String;
                    var serviceMinOrder = dict.valueForKey("sp_min_order") as! String;
                    var serviceNum = dict.valueForKey("sp_number") as! String;
                    var serviceType = dict.valueForKey("sp_type") as! String;

                    var providerObj = NZServiceProvider(spID: serviceId, spCode: serviceCode, spName: serviceName, spNumber: serviceNum, spType: serviceType, spDel: serviceDelTime, spMinOrder: serviceMinOrder)
                    self.serviceProviderArray.addObject(providerObj);

                }
                if(self.serviceProviderArray.count>0){
                    self.lblSellersNotFound.hidden = true
                }
                else{
                    self.lblSellersNotFound.hidden = false
                }
                 self.tblStore.reloadData();
                MBProgressHUD.hideHUDForView(self.view, animated: true);
                
            },
            failure: { (operation: AFHTTPRequestOperation!,
                error: NSError!) in
                println("Error: " + error.localizedDescription)
                MBProgressHUD.hideHUDForView(self.view, animated: true);
                Helper.showAlert("Not Found Near Store!", message: "Please Select Valid Address", delegate: self, cancelButtonTitle: "Ok")
                
        })
        
       

    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        if(stCode == 7){
          self.selectedImage.sd_setImageWithURL(NSURL(string: "\(HOME_IMAGE_URL_FLOWER_FULL)"), placeholderImage: UIImage(named: ""));
        }
        else{
        self.selectedImage.sd_setImageWithURL(NSURL(string: "\(HOME_IMAGE_URL_FULLIMAGES)\(stCode)"), placeholderImage: UIImage(named: ""));
        }
    }
    
    
//    override func viewDidAppear(animated: Bool) {
//        if(serviceProviderArray.count>0){
//            self.lblSellersNotFound.hidden = true
//        }
//        else{
//            self.lblSellersNotFound.hidden = false
//        }
//    }

    init(imgName:String,imgNo:Int,controller:UIViewController) {
        //imageData = data;
        imggName = imgName
        stCode = imgNo+1
        contro = controller;
        super.init(nibName: "StoreViewController", bundle: nil);
    }
    
    
    
    //***** TableView Delegates *****//
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return serviceProviderArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        NSBundle.mainBundle().loadNibNamed("StoreViewCell", owner: self, options: nil);
        var cell:StoreViewCell=storeCustomCell;
        
        cell.bindServiceObject(self.serviceProviderArray.objectAtIndex(indexPath.row) as! NZServiceProvider);
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var spObject:NZServiceProvider = self.serviceProviderArray.objectAtIndex(indexPath.row) as! NZServiceProvider;
        Helper.saveStringInDefault(SP_CODE, value: spObject.sp_code);
        Helper.saveStringInDefault(ST_CODE, value: "\(stCode)");
        Helper.saveStringInDefault(SELLER_MIN_ORDER, value: spObject.sp_min_order);
        let controller = PageMenuViewController(stCode: stCode, spCode: spObject.sp_code.toInt()!);
        self.navigationController!.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func backButtonAction(sender: AnyObject) {
        
        let controller = HomeViewController(nibName: "HomeViewController", bundle: nil)
        self.navigationController!.pushViewController(controller, animated: true)
        
    }
    
    
    
}
