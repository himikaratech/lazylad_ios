//
//  ShoppingCartViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/12/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class ShoppingCartViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    @IBOutlet weak var btnTopMenu: UIButton!
    @IBOutlet var customCell: ShoppingCartViewCell!
    @IBOutlet weak var tblShopping: UITableView!
    var totalSelectedItems:NSMutableArray!;
    var menuView:MenuViewController!;
    var keyArray:NSMutableArray!;
    override func viewDidLoad() {
        super.viewDidLoad()
        keyArray = NSMutableArray();
        //addMenuView();
        //gestuer();
        ShowingSelectedItems();
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func ShowingSelectedItems(){
        totalSelectedItems = NSMutableArray();
        var itemCount:Int = 0;
        var totalItemPrice:Int = 0;
        
        var itemDict = NSMutableDictionary();
        var itemAddArray = NSMutableArray();
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            itemDict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
            
            for (key,value) in itemDict {
                keyArray.addObject(key);
                println("Key is:\(key)  and value:\(value)");
                var itemArray = itemDict.valueForKey("\(key)") as! NSMutableArray;
                itemCount += itemArray.count;
                totalSelectedItems.addObjectsFromArray(itemArray as [AnyObject]);
                for(var i=0 ; i<itemArray.count;++i ){
                    var itemObj = itemArray.objectAtIndex(i) as! NZItemsServiceProviderCategoryWise;
                    
                }
                
            }
            
            self.tblShopping.reloadData();
            
        }
        
    }

    

    //***** TableView Delegates *****//
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            return Helper.fetchObjectDict(SELECTED_ITEM_KEY).count;
        }
        else{
            return 0;
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        NSBundle.mainBundle().loadNibNamed("ShoppingCartViewCell", owner: self, options: nil);
        var cell:ShoppingCartViewCell=customCell;
        var dict = NSMutableDictionary();
        var itemCount:Int;
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            dict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
            
                var itemArray = dict.valueForKey("\(keyArray.objectAtIndex(indexPath.row))") as! NSMutableArray;
                itemCount = itemArray.count;
                totalSelectedItems.addObjectsFromArray(itemArray as [AnyObject]);
                    var itemObj = itemArray.objectAtIndex(0) as! NZItemsServiceProviderCategoryWise;
            
            cell.bindDataObject(itemObj, itemNo: itemCount);
        }
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
    }


    @IBAction func clickToMenu(sender: AnyObject) {
        //self.navMenuShow();
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    
    //****************************** Showing Menu View Code With Gesture ********************************
    
    // Gesture Implement Here
    func gestuer(){
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                self.navMenuShow();
            case UISwipeGestureRecognizerDirection.Left:
                self.navMenuHide();
            default:
                break
            }
        }
    }
    

    // Add Menu View As a Subview
    func addMenuView(){
        menuView = MenuViewController(title: "Home", controller: self);
        self.view.addSubview(menuView.view);
        menuView.view.frame = CGRectMake(-320, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);
        
    }
    
    func navMenuHide()
    {
        UIView.animateWithDuration(0.5, animations: {
            self.menuView.view.frame = CGRectMake(-320, 0, self.menuView.view.frame.size.width, self.menuView.view.frame.size.height);
            self.btnTopMenu.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
            
        })
    }
    func navMenuShow()
    {
        UIView.animateWithDuration(0.5, animations: {
            self.menuView.view.frame = CGRectMake(0, 0, self.menuView.view.frame.size.width, self.menuView.view.frame.size.height);
            self.btnTopMenu.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
            
        })
        
    }

}
