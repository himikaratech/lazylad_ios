//
//  CheckOutViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/5/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

protocol CheckOutProtocol{
    func animateScreen(isUp: Bool);
}
class CheckOutViewController:  UIViewController, UITableViewDataSource, UITableViewDelegate,CheckOutCellDelegate {
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBOutlet var customCell: CheckOutCell!
    @IBOutlet var btnCheckout: UIButton!
    @IBOutlet var btnViewOpenClose: UIButton!
    @IBOutlet var lblItemCount: UILabel!
    @IBOutlet var lblItemCost: UILabel!
    @IBOutlet var itemsTableView: UITableView!
    var controler:UIViewController!;
    var delegate: CheckOutProtocol?;
    var keyArray:NSMutableArray!;
    var totalSelectedItemObjectArray:NSMutableArray!;
    override func viewDidLoad() {
        super.viewDidLoad()
        keyArray = NSMutableArray();
        totalSelectedItemObjectArray = NSMutableArray();
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("notifyItemSelected"), name: "SELECTED_ITEM_NOTIFY", object: nil);
        // self.itemsTableView.reloadData();
        
        // Do any additional setup after loading the view.
    }
    
    init(title:String,controller:UIViewController) {
        self.controler=controller;
        super.init(nibName: "CheckOutViewController", bundle: nil);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //***** TableView Delegates *****//
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            return Helper.fetchObjectDict(SELECTED_ITEM_KEY).count;
        }
        else{
            return 0;
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        NSBundle.mainBundle().loadNibNamed("CheckOutCell", owner: self, options: nil);
        var cell:CheckOutCell=customCell;
        tableView.allowsSelection = false;
        var itemObj:NZItemsServiceProviderCategoryWise = totalSelectedItemObjectArray.objectAtIndex(indexPath.row) as! NZItemsServiceProviderCategoryWise;
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            var dict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
            var itemArray = NSMutableArray();
            println("Key is:\(keyArray.objectAtIndex(indexPath.row)) ");
            itemArray = dict.valueForKey("\(keyArray.objectAtIndex(indexPath.row))") as! NSMutableArray;
            var itemCount = itemArray.count;
            var itemObj = itemArray.objectAtIndex(0) as! NZItemsServiceProviderCategoryWise;
            cell.bindDataWithitemsServiceProvider(itemObj, itemNum: itemCount);
        }
        
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
    }
    /*
    //   Fired Notification From Selected Item
    //    Get All Selected Items
    
    */
    func notifyItemSelected(){
        totalSelectedItemObjectArray = NSMutableArray();
        keyArray = NSMutableArray();
        var itemCount:Int = 0;
        var totalItemPrice:Int = 0;
        
        var itemDict = NSMutableDictionary();
        var itemAddArray = NSMutableArray();
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            itemDict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
            
            for (key,value) in itemDict {
                keyArray.addObject(key);
                println("Key is:\(key)  and value:\(value)");
                var itemArray = itemDict.valueForKey("\(key)") as! NSMutableArray;
                itemCount += itemArray.count;
                totalSelectedItemObjectArray.addObjectsFromArray(itemArray as [AnyObject]);
                for(var i=0 ; i<itemArray.count;++i ){
                    var itemObj = itemArray.objectAtIndex(i) as! NZItemsServiceProviderCategoryWise;
                    totalItemPrice += itemObj.is_item_cost;
                }
                
            }
            self.lblItemCost.text = "₹ \(totalItemPrice)"
            self.lblItemCount.text = "\(itemCount)"
            self.itemsTableView.reloadData();
            
        }
        else{
            self.itemsTableView.reloadData();
            self.lblItemCost.text = "₹ 0"
            self.lblItemCount.text = "0"
        }
        NSNotificationCenter.defaultCenter().postNotificationName("CHECKOUT_ITEM_NOTIFY", object: nil);
        
        
        
    }
    
    
    
    func tableReload(){
        self.itemsTableView.reloadData();
    }
    
    
    @IBAction func checkoutButtonAction(sender: AnyObject) {
        if( self.lblItemCount.text?.toInt()>0){
        var addressView = AddressListViewController(nibName: "AddressListViewController", bundle: nil)
        addressView.isComeFromCheckOut = true;
        self.controler.navigationController?.pushViewController(addressView, animated: true);
        }
        else{
            Helper.showAlert("No Item Selected", message: "Please Selected atleast 1 Item", delegate: self, cancelButtonTitle: "Ok");
        }
        
    }
    
    @IBAction func openCloseButtonAction(sender: UIButton) {
        sender.selected = !sender.selected;
        if(!sender.selected){
            //delegate?.animateScreen(false);
            NSNotificationCenter.defaultCenter().postNotificationName("GoToDOWN", object: nil);
            
        }
        else{
            //delegate?.animateScreen(true);
            NSNotificationCenter.defaultCenter().postNotificationName("GoToUP", object: nil);
            
        }
        
    }
}
