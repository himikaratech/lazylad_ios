//
//  PlaceOrderViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/5/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class PlaceOrderViewController: UIViewController {

    @IBOutlet var imgPlaceOrder: UIImageView!
    @IBOutlet var btnContinueShopping: UIButton!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var btnRateUs: UIButton!
    
    @IBOutlet var imgMedical: UIImageView!
    @IBOutlet var imghome: UIImageView!
    @IBOutlet var imgHouse: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imghome.hidden = false
        self.imgHouse.hidden = true
        self.imgMedical.hidden = true

        NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "startTimer", userInfo: nil, repeats: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func startTimer(){
        self.imghome.hidden = true
        self.imgHouse.hidden = false
        self.imgMedical.hidden = true
        NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "startTimer2", userInfo: nil, repeats: false)
    }
    
    func startTimer2(){
        self.imghome.hidden = true
        self.imgHouse.hidden = true
        self.imgMedical.hidden = false
        NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "viewDidLoad", userInfo: nil, repeats: false)
    }

    @IBAction func continueShoppingBtnAction(sender: AnyObject) {
        let controller = HomeViewController(nibName: "HomeViewController", bundle: nil)
        self.navigationController!.pushViewController(controller, animated: true)
    }

    
    @IBAction func rateUsBtnAction(sender: AnyObject) {
    }
    

    @IBAction func shareBtnAction(sender: AnyObject) {
        let activityViewController = UIActivityViewController(
            activityItems: ["Try this app,its great - Free your evenings and weekends of daily needs shooping with LazyLad. Download it from AppStore "],
            applicationActivities: nil)
        
        presentViewController(activityViewController, animated: true, completion: nil)
    }

    
    
    
}
