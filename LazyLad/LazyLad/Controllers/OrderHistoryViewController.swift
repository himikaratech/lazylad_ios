//
//  OrderHistoryViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/8/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class OrderHistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,orderHistoryDelegate {

    @IBOutlet weak var btnTopNav: UIButton!

    @IBOutlet weak var tblOrder: UITableView!
    var menuView:MenuViewController!;
    @IBOutlet var orderCustomCell: OrderHistoryCell!

    var previousOrderArray:NSMutableArray!;
    var previousConfirmedOrderArray:NSMutableArray!;

    override func viewDidLoad() {
        super.viewDidLoad()
        previousOrderArray = NSMutableArray();

        self.btnTopNav.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
        gestuer();
        addMenuView();
        if(IJReachability.isConnectedToNetwork()){
            callpreviousOrderApi();
        }
        else{
            Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
        }
        
    }

    override func viewDidAppear(animated: Bool) {
       // addressObjectArray = Helper.fetchObjectArray(SAVE_ADDRESS_OBJECT);
        self.tblOrder.reloadData();
    }

    
    func addMenuView(){
        menuView = MenuViewController(title: "Home", controller: self);
        self.view.addSubview(menuView.view);
        menuView.view.frame = CGRectMake(-320, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);
    }
    
    
    // Gesture Implement Here
    func gestuer(){
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                self.navMenuShow();
            case UISwipeGestureRecognizerDirection.Left:
                self.navMenuHide();
            default:
                break
            }
        }
    }
    
    
    func navMenuHide()
    {
        UIView.animateWithDuration(0.5, animations: {
            self.menuView.view.frame = CGRectMake(-320, 0, self.menuView.view.frame.size.width, self.menuView.view.frame.size.height);
            self.btnTopNav.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
            
        })
    }
    func navMenuShow()
    {
        UIView.animateWithDuration(0.5, animations: {
            self.menuView.view.frame = CGRectMake(0, 0, self.menuView.view.frame.size.width, self.menuView.view.frame.size.height);
            self.btnTopNav.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
            
        })
        
    }
    

    @IBAction func slideMenuBtnAction(sender: UIButton) {
        sender.selected = !sender.selected;
        self.navMenuShow()
        //        if(!sender.selected){
        //            self.navMenuHide()
        //        }
        //        else{
        //            self.navMenuShow()
        //        }
    }

    //***** TableView Delegates *****//
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return previousOrderArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        NSBundle.mainBundle().loadNibNamed("OrderHistoryCell", owner: self, options: nil);
        var cell:OrderHistoryCell=orderCustomCell;
        cell.selectionStyle = UITableViewCellSelectionStyle.None;
        cell.delegate = self
        if previousOrderArray.count>0{
        var itemObj:NZPreviousOrder = previousOrderArray.objectAtIndex(indexPath.row) as! NZPreviousOrder;
            cell.bindDataOrderHistoryObjet(itemObj);
        }
        else{}
        
        return cell

        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
//        let controller = OrderHistoryPageMenu()
//        self.navigationController?.pushViewController(controller, animated: true)
         var itemObj:NZPreviousOrder = previousOrderArray.objectAtIndex(indexPath.row) as! NZPreviousOrder;
        var orderMenuScreen = OrderHistoryPageMenu(oderObj: itemObj);
        self.navigationController?.pushViewController(orderMenuScreen, animated: true);
       
    }
    
    // Call Back Delegate
    func clickToBillView(ordrCode: String) {
        var billView = OrderBillViewController(orderCode: ordrCode)
        self.navigationController?.pushViewController(billView, animated: true);
    }
    
    
    // ********************* Previous Order API ************************

    func callpreviousOrderApi(){
        MBProgressHUD.showHUDAddedTo(self.view, animated: true);
        var areaCode = Helper.fetchString(AREA_CODE) as! String;
        let userCode = Helper.fetchString(USER_ID) as! String
        let manager = AFHTTPRequestOperationManager()
        manager.GET(
        "\(BASEURL)getUserPreviousOrdersGSON/\(userCode)",
        parameters: nil,
            success: { (operation: AFHTTPRequestOperation!,
                responseObject: AnyObject!) in
                println("JSON: " + responseObject.description)
                
                let results: NSArray = (responseObject["previous_orders_details"] as? NSArray)!
                var dict:NSDictionary = NSDictionary();
                for (var i=0; i<results.count;++i){
                dict = results[i] as! NSDictionary;
                    var order_code = dict.valueForKey("order_code") as! String;
                    var order_date = dict.valueForKey("order_date") as! String;
                    var order_amount = dict.valueForKey("order_amount") as! Int;
                    var order_status = dict.valueForKey("order_status") as! String;
                    var sp_name = dict.valueForKey("sp_name") as! String;



                    var previoisObj = NZPreviousOrder(odrCode: order_code, odrDate: order_date, odrAmount: order_amount, odrStatus: order_status, spName: sp_name)
                    self.previousOrderArray.addObject(previoisObj);
                    
                }
                MBProgressHUD.hideHUDForView(self.view, animated: true);
            },
            failure: { (operation: AFHTTPRequestOperation!,
                error: NSError!) in
                println("Error: " + error.localizedDescription)
        })
    }

    
    // Call Back Delegate
    func tableReloadFromCancel(){
        self.tblOrder.reloadData();
    }
    
    
    
    // ********************* getConfirmedPreviousOrderDetails API ************************
    /*
    func getConfirmedPreviousOrderDetailsApi(){
        
        var orderCode = Helper.fetchString(ORDER_CODE) as! String;
        let userCode = Helper.fetchString(USER_ID) as! String
        let manager = AFHTTPRequestOperationManager()
        manager.GET(
            "\(BASEURL)getConfirmedPreviousOrderDetails/\(orderCode)/\(userCode)",
            parameters: nil,
            success: { (operation: AFHTTPRequestOperation!,
                responseObject: AnyObject!) in
                println("JSON: " + responseObject.description)
                
                let results: NSArray = (responseObject["previous_orders_confirmed_details"] as? NSArray)!
                var dict:NSDictionary = NSDictionary();
                for (var i=0; i<results.count;++i){
                    dict = results[i] as! NSDictionary;
                    var order_code = dict.valueForKey("item_code") as! String;
                    var order_date = dict.valueForKey("odrDate") as! String;
                    var order_amount = dict.valueForKey("odrAmount") as! Int;
                    var order_status = dict.valueForKey("odrStatus") as! String;
                    var sp_name = dict.valueForKey("spName") as! String;
                    
                    
                    
                    var previoisObj = NZPreviousConfirmedOrder(odrCode: order_code, odrDate: order_date, odrAmount: order_amount, odrStatus: order_status, spName: sp_name)
                    self.previousConfirmedOrderArray.addObject(previoisObj);
                    
                }
                
            },
            failure: { (operation: AFHTTPRequestOperation!,
                error: NSError!) in
                println("Error: " + error.localizedDescription)
        })
    }
*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


   
}
