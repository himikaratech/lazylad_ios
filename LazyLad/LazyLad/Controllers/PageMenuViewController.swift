//
//  PageMenuViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/8/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class PageMenuViewController: UIViewController, CAPSPageMenuDelegate, UITableViewDataSource, UITableViewDelegate {
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @IBOutlet var btnSearchCancel: UIButton!
    @IBOutlet var searchView: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var vwSearch: UIView!
    @IBOutlet weak var tblSearch: UITableView!
    var checkOutView:CheckOutViewController!;
    var pageMenu : CAPSPageMenu?
    var pageMenu1 : CAPSPageMenu?
    var st_Code:Int!;
    var sp_Code:Int!;
    var isNextItem:Bool!;
    var incrementNum = 0;
    var isSearch:Bool!;
    var serviceCategoryArray:NSMutableArray!;
    var getArrayFromDict:NSMutableArray!;
    var itemServiceArray:NSMutableArray!;
    var fullArrayforSearchResult:NSMutableArray!;
    var itemServiceDict:NSMutableDictionary!;
    var searchArray:NSMutableArray!;
    var shopView:ShopViewController!;
    override func viewDidLoad() {
        super.viewDidLoad()
        searchArray = NSMutableArray();
        itemServiceDict = NSMutableDictionary();
        getArrayFromDict = NSMutableArray();
        fullArrayforSearchResult = NSMutableArray();
        self.tblSearch.backgroundColor = UIColor.blackColor();
        
        //self.btnSearch.imageView?.image = UIImage(named: "search-icon");
        self.searchView.hidden = true;
        self.vwSearch.hidden = true;
        self.txtSearch.hidden = true;
        Helper.removeUserDefault(SELECTED_ITEM_KEY);
        if(IJReachability.isConnectedToNetwork()){
            callCategoryApi();
        }
        else{
            Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
        }
        
        
         //addSubViewInBottom();
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "gotoUp", name: "GoToUP", object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "gotoDown", name: "GoToDOWN", object: nil);

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    init(stCode:Int, spCode:Int) {
        st_Code = stCode;
        sp_Code = spCode;
        super.init(nibName: "PageMenuViewController", bundle: nil);
    }

    override func viewWillAppear(animated: Bool) {
    self.txtSearch.text = "";
        self.txtSearch.resignFirstResponder();
        self.vwSearch.hidden = true;
    
    }
    
    
    // ********************* ServiceCategories API ************************
    func callCategoryApi(){
        MBProgressHUD.showHUDAddedTo(self.view, animated: true);
        
        serviceCategoryArray = NSMutableArray();
        let manager = AFHTTPRequestOperationManager();
        var params = ["st_code":st_Code]
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.requestSerializer = AFHTTPRequestSerializer();
        manager.POST("\(BASEURL)serviceCategories", parameters: params, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
            println("Yes thies was a success")
            var error: NSError?
            let jsonData: NSData = responseObject as! NSData;
            let jsonDict = NSJSONSerialization.JSONObjectWithData(jsonData, options: nil, error: &error)as! NSDictionary
             println("JSON:\(jsonDict)")
            
            let results: NSArray = (jsonDict["service_categories"] as? NSArray)!
            
            var dict:NSDictionary = NSDictionary();
            for (var i=0; i<results.count;++i){
                println("For Loop Num :\(i)")
                dict = results[i] as! NSDictionary;
                var serviceId = dict.valueForKey("id") as! Int;
                var serviceName = dict.valueForKey("sc_name") as! String;
                var serviceCode = dict.valueForKey("sc_code") as! String;
                var categoryCode = dict.valueForKey("st_code") as! String;
    
                var categoryObj = NZServiceCategory(scID: serviceId, stCode: categoryCode, scCode: serviceCode, scName: serviceName);
                self.serviceCategoryArray.addObject(categoryObj);
                //self.callItemApi(self.sp_Code, stCode: self.st_Code, scCode: serviceCode.toInt()!, serviceNameWithIndex: "Index\(i)");
                
                
                
            }
            
            self.apiCallForItems();
            },
            
            failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                println("We got an error here.. \(error.localizedDescription)")
        })
        
    }
    
    func apiCallForItems(){
        
        if(incrementNum < serviceCategoryArray.count){
      var categoryObjdata:NZServiceCategory = serviceCategoryArray.objectAtIndex(incrementNum) as! NZServiceCategory;
            if(IJReachability.isConnectedToNetwork()){
                 self.callItemApi(self.sp_Code, stCode: self.st_Code, scCode: categoryObjdata.sc_code.toInt()!, serviceNameWithIndex: "Index\(incrementNum)");
            }
            else{
                Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
            }
       
        }
        else{
            for(key,value) in self.itemServiceDict{
            fullArrayforSearchResult.addObjectsFromArray(self.itemServiceDict.objectForKey(key) as! NSArray as! [AnyObject])
            MBProgressHUD.hideHUDForView(pageMenu?.view, animated: true);
            }
            
        }
        
    }
    
    // ********************* ItemsServiceProviderCategoryWiseFinalWithMRP API ************************

    func callItemApi(spCode:Int,stCode:Int,scCode:Int, serviceNameWithIndex:String){

        

        self.itemServiceArray = NSMutableArray();
        
        var areaCode = Helper.fetchString(AREA_CODE) as! String;
        
        let manager = AFHTTPRequestOperationManager()
        
        manager.GET("\(BASEURL)itemsServiceProviderCategoryWiseFinalWithMRP/\(spCode)/\(stCode)/\(scCode)",
            
            parameters: nil,
            
            success: { (operation: AFHTTPRequestOperation!,
                
                responseObject: AnyObject!) in
                
                println("JSON: " + responseObject.description)
                
                
                
                let results: NSArray = (responseObject["items_service_providers"] as? NSArray)!
                
                
                
                var dict:NSDictionary = NSDictionary();
                
                
                
                for (var i=0; i<results.count;++i){
                    
                    dict = results[i] as! NSDictionary;
                    
                    var itemID = dict.valueForKey("id") as! Int;
                    
                    var itemCode = dict.valueForKey("item_code") as! String;
                    
                    var spCode = dict.valueForKey("sp_code") as! String;
                    
                    var item_type_code = dict.valueForKey("item_type_code") as! String;
                    
                    var itemName = dict.valueForKey("item_name") as! String;
                    
                    var itemUnit = dict.valueForKey("item_unit") as! String;
                    
                    var itemQuantity = dict.valueForKey("item_quantity") as! String;
                    
                    var itemCost = dict.valueForKey("item_cost") as! Int;
                    
                    var itemimgflag = dict.valueForKey("item_img_flag") as! Int;
                    
                    var itemimgaddress = dict.valueForKey("item_img_address") as! String;
                    
                    var itemshortdesc = dict.valueForKey("item_short_desc") as! String;
                    
                    var itemdesc = dict.valueForKey("item_desc") as! String;
                    
                    var itemstatus = dict.valueForKey("item_status") as! Int;
                    
                    var itemselected = dict.valueForKey("item_selected") as! Int;
                    
                    var itemquantityselected = dict.valueForKey("item_quantity_selected") as! Int;
                    
                    var itemservicetype = dict.valueForKey("item_service_type") as! String;
                    
                    var itemservicecategory = dict.valueForKey("item_service_category") as! String;
                    
                    var itemmrp = dict.valueForKey("item_mrp") as! Int;
                    
                    
                    
                    var itemService = NZItemsServiceProviderCategoryWise(isID: itemID, isItemCode: itemCode, isSpCode: spCode, isItemTypeCode: item_type_code, isItemName: itemName, isItemUnit: itemUnit, isItemQuantity: itemQuantity, isItemCost: itemCost, isItemImg_flag: itemimgflag, isItemImg_address: itemimgaddress, isItemShort_desc: itemshortdesc, isItemDesc: itemdesc, isItemStatus: itemstatus, isItemSelected: itemselected, isItemQuantity_selected: itemquantityselected, isItemService_type: itemservicetype, isItemService_category: itemservicecategory, isItemMRP: itemmrp)
                    
                    self.itemServiceArray.addObject(itemService);
                    
                    //Remove All Selected Items
                    
                    // Helper.removeUserDefault(itemCode);
                    
                    
                    
                    
                    
                }
                
                
                
                self.itemServiceDict.setObject(self.itemServiceArray, forKey: serviceNameWithIndex);
                
                
                
                if(serviceNameWithIndex == "Index0"){
                    
                    self.paggingScrooling();
                    
                    self.addSubViewInBottom();
                    
                    self.getArrayFromDict = self.itemServiceDict.objectForKey("Index0") as! NSMutableArray;
                    
                    //self.getDataFromDic(0, cont: self);
                    
                }
                
                println("Item Api Success :\(serviceNameWithIndex)")
                
                self.incrementNum+=1;
                
                
                
                self.apiCallForItems();
                
            },
            
            failure: { (operation: AFHTTPRequestOperation!,
                
                error: NSError!) in
                
                println("Error: " + error.localizedDescription)
                
        })
        

        

        

        

        

    }


    // Table View Delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return searchArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
        
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "mycell")
         var itemObj:NZItemsServiceProviderCategoryWise = searchArray.objectAtIndex(indexPath.row) as! NZItemsServiceProviderCategoryWise;
        cell.backgroundColor = UIColor.blackColor();
        cell.textLabel?.textColor = UIColor.whiteColor();
        tableView.separatorColor = UIColor.whiteColor();
        cell.textLabel?.text = itemObj.is_item_name;
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        tableView.deselectRowAtIndexPath(indexPath, animated: true);
        var itemObj:NZItemsServiceProviderCategoryWise = searchArray.objectAtIndex(indexPath.row) as! NZItemsServiceProviderCategoryWise;
        var itemName = itemObj.is_item_name;
        var filterArray:NSArray = Helper.getSearchList(itemName, searchList: fullArrayforSearchResult, searchObject: "is_item_name");
       // var searchView = SearchViewController(ItemArray: filterArray, controller: self);
        var searchView = SearchViewController(ItemArray: filterArray, fullListArray: searchArray, controller: self, iskeyBoardDone:false)
        self.navigationController?.pushViewController(searchView, animated: true);
       
        
        
    }
    
    
    
    func addSubViewInBottom(){
        checkOutView = CheckOutViewController(title: "checkout", controller: self);
        //checkOutView?.delegate=self;
        
        self.view.addSubview(checkOutView.view);
        self.view.insertSubview(checkOutView.view, aboveSubview:pageMenu!.view );
        checkOutView.view.frame = CGRectMake(0, self.view.frame.height-50, checkOutView.view.frame.size.width, checkOutView.view.frame.size.height)
       ;
       
        
    }
    
    func gotoUp(){
        UIView.animateWithDuration(0.5, animations: {
            self.checkOutView.view.frame = CGRectMake(0, 50, self.checkOutView.view.frame.size.width,self.checkOutView.view.frame.size.height);
        })
        //NSNotificationCenter.defaultCenter().removeObserver(self);
    }
    
    func gotoDown(){
        UIView.animateWithDuration(0.5, animations: {
            self.checkOutView.view.frame = CGRectMake(0, self.view.frame.height-50, self.checkOutView.view.frame.size.width, self.checkOutView.view.frame.size.height)
            
        })
        //NSNotificationCenter.defaultCenter().removeObserver(self);
        
    }
    
    
    
    
    
    // Make Pagging Scrooling
    
    func paggingScrooling(){
        
        
        var controllerViewArray : [UIViewController] = []
        var ArrayData:NSMutableArray!;
        MBProgressHUD.hideHUDForView(self.view, animated: true);
        for (var i = 0; i < serviceCategoryArray.count; i++)
        {
            
            var shopContr = ShopViewController(objArray: itemServiceArray, controller: self);
            var categoryObj:NZServiceCategory = serviceCategoryArray.objectAtIndex(i) as! NZServiceCategory;
            shopContr.title = categoryObj.sc_name;
            controllerViewArray.append(shopContr)
        }
             // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerViewArray, frame: CGRectMake(0.0, 50.0, self.view.frame.width, self.view.frame.height-50), pageMenuOptions: nil)
        pageMenu?.delegate = self;
        println(self.view.frame.height)
        self.view.addSubview(pageMenu!.view)
         MBProgressHUD.showHUDAddedTo(pageMenu!.view, animated: true);
        
       
    }
    
    @IBAction func clickToBack(sender: AnyObject) {
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
        var refreshAlert = UIAlertController(title: "Alert", message: "Shopping cart will become empty on going back.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popViewControllerAnimated(true);
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        
        presentViewController(refreshAlert, animated: true, completion: nil)
        }
        else{
            self.navigationController?.popViewControllerAnimated(true);
        }
      
       
        
    }
    
    
    @IBAction func clickToSearch(sender: UIButton) {
            self.searchView.hidden = false
            self.txtSearch.hidden = false;
            self.btnSearch.hidden = true;
            self.btnSearch.imageView?.image = UIImage(named: "cancel-icon");
            self.txtSearch.becomeFirstResponder()
    }

    @IBAction func clickToCancelSearch(sender: AnyObject) {
        self.txtSearch.hidden = true;
        self.txtSearch.text = "";
        self.btnSearch.hidden = false;
        self.searchView.hidden = true;
        self.vwSearch.hidden = true;
        self.txtSearch.resignFirstResponder()
        
    }
    
    //CAPSPageMenu Delegate Method
    func willMoveToPage(controller: UIViewController, index: Int){
        getDataFromDic(index, cont: controller);
        
    }
    
    func didMoveToPage(controller: UIViewController, index: Int){
        println("didMoveToPagedidMoveToPagedidMoveToPagedidMoveToPage");
    }
    
    // Get Data
    func getDataFromDic(indexx:Int, cont:UIViewController){
        var dictKey = "Index\(indexx)";
        getArrayFromDict = itemServiceDict.objectForKey(dictKey) as! NSMutableArray;
        var shopViewController:ShopViewController = cont as! ShopViewController;
        shopViewController.reloadTable(getArrayFromDict);
    }
    
    
    
    // UIText Field Delegates for Searching
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let replacedText = (textField.text as NSString)
            .stringByReplacingCharactersInRange(range, withString: string);
        searchTableList(replacedText);
        return true;
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder();
        var filterArray:NSArray = NSArray();
        // var searchView = SearchViewController(ItemArray: filterArray, controller: self);
        var searchView = SearchViewController(ItemArray: filterArray, fullListArray: searchArray, controller: self, iskeyBoardDone:true)
        self.navigationController?.pushViewController(searchView, animated: true);
        return true;
    }
    
    
    func searchTableList(txtData:String){
        if(txtData.isEmpty){
            isSearch = false;
            self.txtSearch.text = "";
            self.vwSearch.hidden = true;
            self.txtSearch.resignFirstResponder();
        }
        else{
            self.vwSearch.hidden = false;
            self.view.bringSubviewToFront(self.vwSearch);
            isSearch = true;
            var filterArray = Helper.getSearchList(txtData, searchList: fullArrayforSearchResult, searchObject: "is_item_name");
             searchArray = NSMutableArray(array: filterArray);
            
           
            
        }
        self.tblSearch .reloadData();
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
