//
//  AddAddressViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/5/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class AddAddressViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var scrllView: UIScrollView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtHouseNo: UITextField!
    @IBOutlet weak var txtSubArea: UITextField!
    
    @IBOutlet var txtAreaCity: UITextField!
    var user_code:String!
    var customer_name:String!
    var customer_email:String!
    var customer_address:String!
    var user_Code:Int!;
    var user_Address_code:Int!;
    
    var addressArray:NSMutableArray!
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);

        var currentArea = (Helper.fetchString(CURRENT_AREA) as! String);
        var currentCity = (Helper.fetchString(CURRENT_CITY) as! String);
        var fullAdrs = "\(currentArea), \(currentCity)";
        self.txtAreaCity.text = fullAdrs;
        //addNewAddressApi();
        txtPhoneNo.numberKeybord();
        // Do any additional setup after loading the view.
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        var contentInsets:UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardFrame.height, right: 0.0);
        
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.scrllView.contentInset = contentInsets;
            self.scrllView.contentSize = CGSizeMake(self.scrllView.frame.size.width, self.scrllView.frame.height-keyboardFrame.height)
        })
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        var contentInsets:UIEdgeInsets = UIEdgeInsetsZero;
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.scrllView.contentInset = contentInsets;
            self.scrllView.contentSize = CGSizeMake(self.scrllView.frame.size.width, self.scrllView.frame.height+keyboardFrame.height)
        })
    }
    
    
   override func viewWillDisappear(animated: Bool) {
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil);
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil);
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickToCancel(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true);
  
    }
    
    @IBAction func clickToAdd(sender: AnyObject) {
        if(self.txtName.text.isEmpty){
            Helper.showAlert("Empty", message: "Please Enter Name", delegate: self, cancelButtonTitle: "Ok");
        }
        else if(self.txtPhoneNo.text.isEmpty){
            Helper.showAlert("Empty", message: "Please Enter Phone No", delegate: self, cancelButtonTitle: "Ok");
        }
        else if(self.txtHouseNo.text.isEmpty){
            Helper.showAlert("Empty", message: "Please Enter House No", delegate: self, cancelButtonTitle: "Ok");
        }
            
        else if(self.txtSubArea.text.isEmpty){
            Helper.showAlert("Empty", message: "Please Enter SubArea", delegate: self, cancelButtonTitle: "Ok");
        }

        else if(self.txtAreaCity.text.isEmpty){
            Helper.showAlert("Empty", message: "Please Enter City", delegate: self, cancelButtonTitle: "Ok");
        }
        else{
            if(IJReachability.isConnectedToNetwork()){
                callCategoryApi();
            }
            else{
                Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
            }
        }
        
        

    }
    
    // ********************* Address API ************************
    
    func callCategoryApi(){
        MBProgressHUD.showHUDAddedTo(self.view, animated: true);

        addressArray = NSMutableArray();
        let manager = AFHTTPRequestOperationManager();
        var params = ["user_code":Helper.fetchString(USER_ID) as! String,
            "customer_name":txtName.text,
            "customer_email":"aa@gmail.com",
            "customer_address":"\(txtHouseNo.text)\(txtAreaCity.text)",
            "customer_number":txtPhoneNo.text]

        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.requestSerializer = AFHTTPRequestSerializer();
        manager.POST("\(BASEURL)addNewAddress", parameters: params, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
            println("Yes thies was a success")
            var error: NSError?
            let jsonData: NSData = responseObject as! NSData;
            let jsonDict = NSJSONSerialization.JSONObjectWithData(jsonData, options: nil, error: &error)as! NSDictionary
            println("JSON:\(jsonDict)")
            
            let results: NSArray = (jsonDict["user_details"] as? NSArray)!
            
            var dict:NSDictionary = NSDictionary();
            for (var i=0; i<results.count;++i){
                dict = results[i] as! NSDictionary;
                self.user_Code = dict.valueForKey("user_code") as! Int;
                self.user_Address_code = dict.valueForKey("user_address_code") as! Int;
                
                var addObj = NZAddNewAddress(adUserCode:  self.user_Code, adUserAdrsCode: self.user_Address_code)

                self.addressArray.addObject(addObj);
            }
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            var objArr:NSMutableArray = NSMutableArray();
            //var addressObj = AddressUserPreference(nme: txtName.text, phNo: txtPhoneNo.text, houseNo: txtHouseNo.text, subLocality: txtSubArea.text);
          //  var addressObj = AddressUserPreference(nme: self.txtName.text, phNo: self.txtPhoneNo.text, houseNo: self.txtHouseNo.text, subLocality: self.txtSubArea.text, areaCity:self.txtAreaCity, usercode: "\(self.user_Code)", usercodeAdrs: "\(self.user_Address_code)")
            var addressObj = AddressUserPreference(nme: self.txtName.text, phNo: self.txtPhoneNo.text, houseNo: self.txtHouseNo.text, subLocality: self.txtSubArea.text, city: self.txtAreaCity.text, usercode: "\(self.user_Code)", usercodeAdrs: "\(self.user_Address_code)")
            if(Helper.fetchObjectArray(SAVE_ADDRESS_OBJECT).count>0){
                objArr = Helper.fetchObjectArray(SAVE_ADDRESS_OBJECT);
                
            }
            objArr.addObject(addressObj);
            Helper.saveObjectArray(SAVE_ADDRESS_OBJECT, objArray: objArr);
            
            self.navigationController?.popViewControllerAnimated(true);
            
            },
            
            failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                println("We got an error here.. \(error.localizedDescription)")
        })
        
    }
    
    
    @IBAction func clickToBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    // Text Field Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder();
        return true;
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
