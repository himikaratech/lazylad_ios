//
//  SplashViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/10/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    var addressView:EditAddressViewController!;
    override func viewDidLoad() {
        super.viewDidLoad()
        addAddressView();
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addAddressView(){
        addressView =  EditAddressViewController(isNavigateFromSplash: true, controller: self);
        self.view.addSubview(addressView.view);
    }
    

    @IBAction func clickToHome(sender: AnyObject) {
        var mainViewController = HomeViewController(nibName: "HomeViewController", bundle: nil);
        self.navigationController?.pushViewController(mainViewController, animated: true);
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
