//
//  ShopViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class ShopViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBOutlet var btnScrollView: UIScrollView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnSearch: UIButton!
    @IBOutlet weak var tblItem: UITableView!
    var checkOutView:CheckOutViewController!;
    var menuObjArray:NSMutableArray!;
     var menuObjArray111:NSMutableArray!;
    var viewController:UIViewController!;
    @IBOutlet var shopCustomCell: ShopViewCell!

    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("notifyTableReload"), name: "CHECKOUT_ITEM_NOTIFY", object: nil);
        menuObjArray111=NSMutableArray();
        addSubViewInBottom();
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "gotoUp", name: "GoToUP", object: nil);
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "gotoDown", name: "GoToDOWN", object: nil);
       // self.scrollView()

        // Do any additional setup after loading the view.
    }
    
    init(objArray:NSMutableArray,controller:UIViewController) {
        menuObjArray = NSMutableArray();
        menuObjArray = objArray;
        viewController = controller;
        super.init(nibName: "ShopViewController", bundle: nil);
    }
    
    func reloadTable(arrData:NSMutableArray){
        menuObjArray = NSMutableArray(array: arrData);
        println("MenuItem Count is:\(menuObjArray.count)")
       // self.tblItem.reloadData();
        
    }


    func addSubViewInBottom(){
        checkOutView = CheckOutViewController(title: "checkout", controller: self);
        //checkOutView?.delegate=self;
        //self.view.addSubview(checkOutView.view);
        
        checkOutView.view.frame = CGRectMake(0, self.view.frame.height-50, checkOutView.view.frame.size.width, checkOutView.view.frame.size.height)
        
    }
    
    
    
//    func gotoUp(){
//        UIView.animateWithDuration(0.5, animations: {
//            self.checkOutView.view.frame = CGRectMake(0, 68, self.checkOutView.view.frame.size.width,self.checkOutView.view.frame.size.width);
//        })
//        //NSNotificationCenter.defaultCenter().removeObserver(self);
//    }
//    
//    func gotoDown(){
//        UIView.animateWithDuration(0.5, animations: {
//            self.checkOutView.view.frame = CGRectMake(0, self.view.frame.height-50, self.checkOutView.view.frame.size.width, self.checkOutView.view.frame.size.height)
//            
//        })
//         //NSNotificationCenter.defaultCenter().removeObserver(self);
//        
//    }


    //***** TableView Delegates *****//
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menuObjArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        NSBundle.mainBundle().loadNibNamed("ShopViewCell", owner: self, options: nil);
        var cell:ShopViewCell=shopCustomCell;
         tableView.allowsSelection = false;
        var itemObj:NZItemsServiceProviderCategoryWise = menuObjArray.objectAtIndex(indexPath.row) as! NZItemsServiceProviderCategoryWise;
        cell.bindDataWithitemsServiceProvider(itemObj);
        //cell.lblItemName.text = "Shop"
        return cell
    }
    
    
    
    @IBAction func searchButtonAction(sender: AnyObject) {
    }
    
    @IBAction func backButtonAction(sender: AnyObject) {
//        let controller = StoreViewController(nibName: "StoreViewController", bundle: nil)
//        self.navigationController!.pushViewController(controller, animated: true)
        self.navigationController?.popViewControllerAnimated(true);


    }
    
    func notifyTableReload(){
        self.tblItem.reloadData();
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
