//
//  OrderBillViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/18/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class OrderBillViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @IBOutlet weak var lblName: UILabel!

    @IBOutlet weak var lblDeliveryCharges: UILabel!
    @IBOutlet weak var lblTotalBill: UILabel!
    @IBOutlet weak var lblOrderAmount: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblOrderNum: UILabel!
    @IBOutlet weak var tblOrderDetails: UITableView!
    var order_Code:String!
    var orderItemArray:NSMutableArray!
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrderBillApi();
        orderItemArray = NSMutableArray();
        // Do any additional setup after loading the view.
    }
    
    init(orderCode:String) {
        order_Code = orderCode;
        super.init(nibName: "OrderBillViewController", bundle: nil);
    }

    @IBOutlet var orderCustomCell: OrderBillCell!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //***** TableView Delegates *****//
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
    return orderItemArray.count;
        
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        NSBundle.mainBundle().loadNibNamed("OrderBillCell", owner: self, options: nil);
         var cell:OrderBillCell=orderCustomCell;
        var orderBillObject:NZBillOrderItems = orderItemArray.objectAtIndex(indexPath.row) as! NZBillOrderItems;
        cell.bindOrderBillDetailsObject(orderBillObject);
        return cell;
    }
    
    
    
    // ********************* GetOrderBillForUser API ************************
    func getOrderBillApi(){
        MBProgressHUD.showHUDAddedTo(self.view, animated: true);
        let manager = AFHTTPRequestOperationManager()
        manager.GET(
            "\(BASEURL)/getOrderBillForUser/\(order_Code)",
            parameters: nil,
            success: { (operation: AFHTTPRequestOperation!,
                responseObject: AnyObject!) in
                println("JSON: " + responseObject.description)
                
                // Show Bill Details
                
                self.lblOrderNum.text = responseObject["order_code"] as? String
                self.lblName.text = responseObject["user_name"] as? String
                self.lblAddress.text = responseObject["user_address"] as? String
                self.lblOrderAmount.text = responseObject["order_tot_amount"] as? String
                self.lblTotalBill.text = responseObject["order_total_cost"] as? String
                self.lblDeliveryCharges.text = responseObject["order_delivery_cost"] as? String
                self.lblDiscount.text = responseObject["order_discount_cost"] as? String
                //self.lblOrderNum.text = responseObject["sp_code"] as? String
                let results: NSArray = (responseObject["order_item_details"] as? NSArray)!
                
                var dict:NSDictionary = NSDictionary();
                
                
                for (var i=0; i<results.count;++i){
                    dict = results[i] as! NSDictionary;
                    var itemCode = dict.valueForKey("item_code") as! String;
                    var itemName = dict.valueForKey("item_name") as! String;
                    var itemShortDesc = dict.valueForKey("item_short_desc") as! String;
                    var itemQuantity = dict.valueForKey("item_quantity") as! String;
                    var itemCost = dict.valueForKey("item_cost") as! Int;
                    var itemDesc = dict.valueForKey("item_desc") as! String;
                    var billOrderDetails = NZBillOrderItems(odrCode: itemCode, ordrname: itemName, desc: itemShortDesc, quantity: itemQuantity, cost: itemCost, description: itemDesc);
                    self.orderItemArray.addObject(billOrderDetails);
                    
                }
                self.tblOrderDetails.reloadData();
                MBProgressHUD.hideHUDForView(self.view, animated: true);
                
            },
            failure: { (operation: AFHTTPRequestOperation!,
                error: NSError!) in
                println("Error: " + error.localizedDescription)
        })
        
        
        
    }
    

    
    @IBAction func clickToBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true);
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
