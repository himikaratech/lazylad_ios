//
//  SearchViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/15/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITextFieldDelegate {
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @IBOutlet var btnSearchCancel: UIButton!
    @IBOutlet var searchView: UIView!
    @IBOutlet weak var vwSearch: UIView!
    @IBOutlet weak var tblList: UITableView!
    
    
    var itemServiceArray:NSMutableArray!;
    var fullItemsArray:NSMutableArray!;
    var filterArray:NSMutableArray!;
    var cont:UIViewController!;
    var shopContr:ShopViewController!;
    var isKeyBoard_Done:Bool;
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchView.hidden = true;
        self.txtSearch.hidden = true;
        if(isKeyBoard_Done){
            itemServiceArray = fullItemsArray;
        }
        addStoreAsSubview(itemServiceArray);
        // Do any additional setup after loading the view.
    }
    
    init(ItemArray:NSArray,fullListArray:NSArray,controller:UIViewController, iskeyBoardDone:Bool) {
        
        self.cont=controller;
        isKeyBoard_Done = iskeyBoardDone;
        itemServiceArray = NSMutableArray(array:  ItemArray);
        fullItemsArray = NSMutableArray(array: fullListArray);
        super.init(nibName: "SearchViewController", bundle: nil);
    }
    
    func addStoreAsSubview(arrData:NSMutableArray){
        shopContr = ShopViewController(objArray: arrData, controller: self);
        self.view.addSubview(shopContr.view);
        shopContr.view.frame = CGRectMake(0, 60, shopContr.view.frame.size.width, shopContr.view.frame.size.height);
    }
    
    
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickToSearch(sender: UIButton) {
//        self.btnSearch.selected = !sender.selected;
//        if(self.btnSearch.selected){
            self.txtSearch.hidden = false;
            self.lblTitle.hidden = true;
//        }
//        else{
//            self.txtSearch.hidden = true;
//            self.lblTitle.hidden = false;
//        }
        self.searchView.hidden = false
        self.btnSearch.hidden = true;
        self.btnSearch.imageView?.image = UIImage(named: "cancel-icon");
        self.txtSearch.becomeFirstResponder()
        
    }
    
    
    @IBAction func clickToCancelSearch(sender: AnyObject) {
        self.lblTitle.hidden = false;
        self.txtSearch.hidden = true;
        self.txtSearch.text = "";
        self.btnSearch.hidden = false;
        self.searchView.hidden = true;
        self.vwSearch.hidden = true;
        self.txtSearch.resignFirstResponder()
    
    }
    
    
    
    
    
    // Table View Delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return itemServiceArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
        
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "mycell")
        var itemObj:NZItemsServiceProviderCategoryWise = itemServiceArray.objectAtIndex(indexPath.row) as! NZItemsServiceProviderCategoryWise;
        cell.backgroundColor = UIColor.blackColor();
        cell.textLabel?.textColor = UIColor.whiteColor();
        tableView.separatorColor = UIColor.whiteColor();
        cell.textLabel?.text = itemObj.is_item_name;
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        tableView.deselectRowAtIndexPath(indexPath, animated: true);
        
        var itemObj:NZItemsServiceProviderCategoryWise = itemServiceArray.objectAtIndex(indexPath.row) as! NZItemsServiceProviderCategoryWise;
        var itemName = itemObj.is_item_name;
        var filterDataArray:NSArray = Helper.getSearchList(itemName, searchList: itemServiceArray, searchObject: "is_item_name");
        shopContr.view.removeFromSuperview();
        var dataArr:NSMutableArray = NSMutableArray(array: filterDataArray);
        addStoreAsSubview(dataArr);
        self.vwSearch.hidden = true;
        self.txtSearch.text = "";
        self.txtSearch.resignFirstResponder();

        
    }
    
    
    // UIText Field Delegates for Searching
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let replacedText = (textField.text as NSString)
            .stringByReplacingCharactersInRange(range, withString: string);
        searchTableList(replacedText);
        
        return true;
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder();
        self.vwSearch.hidden = true;
         //self.txtSearch.hidden = true;
        addStoreAsSubview(itemServiceArray);
        
        return true;
    }
    
    
    func searchTableList(txtData:String){
        if(txtData.isEmpty){
            self.vwSearch.hidden = true;
             //self.txtSearch.hidden = true;
        }
        else{
            self.vwSearch.hidden = false;
            self.txtSearch.hidden = false;
            self.view.bringSubviewToFront(self.vwSearch);
            var filterArray = Helper.getSearchList(txtData, searchList: fullItemsArray, searchObject: "is_item_name");
            itemServiceArray = NSMutableArray(array: filterArray);
            
            
            
        }
        self.tblList.reloadData();
    }



    @IBAction func clickToBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
