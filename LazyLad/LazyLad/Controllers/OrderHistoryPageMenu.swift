//
//  OrderHistoryPageMenu.swift
//  LazyLad
//
//  Created by Lazylad on 6/17/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class OrderHistoryPageMenu: UIViewController,CAPSPageMenuDelegate {
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBOutlet var btnBack: UIButton!
     var pageMenu : CAPSPageMenu?
    var previousOrder:NZPreviousOrder!;
    @IBOutlet var lnlTotalAmount: UILabel!
    var itemArray:NSMutableArray!;
    var confirmArray:NSMutableArray!;
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmArray = NSMutableArray();
        lnlTotalAmount.text = "₹ \(previousOrder.order_amount)";
        if(IJReachability.isConnectedToNetwork()){
            callServiceProviderApi();
        }
        else{
            Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
        }
        
        

        // Do any additional setup after loading the view.
    }
    
    init(oderObj:NZPreviousOrder) {
        previousOrder = oderObj;
        super.init(nibName: "OrderHistoryPageMenu", bundle: nil);
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // ********************* Service Provider API ************************
    func callServiceProviderApi(){
        itemArray = NSMutableArray();
        MBProgressHUD.showHUDAddedTo(self.view, animated: true);
        var userCode = Helper.fetchString(USER_ID) as! String;
        let manager = AFHTTPRequestOperationManager()
        manager.GET(
            "\(BASEURL)getUserPreviousOrderDetails/\(previousOrder.order_code)/\(userCode)",
            parameters: nil,
            success: { (operation: AFHTTPRequestOperation!,
                responseObject: AnyObject!) in
                println("JSON: " + responseObject.description)
                
                let results: NSArray = (responseObject["previous_orders_details"] as? NSArray)!
                
                var dict:NSDictionary = NSDictionary();
                
                for (var i=0; i<results.count;++i){
                    dict = results[i] as! NSDictionary;
                    var itemCode = dict.valueForKey("item_code") as! String;
                    var itemName = dict.valueForKey("item_name") as! String;
                    var itemFlag = dict.valueForKey("item_img_flag") as! Int;
                    var itemUrl = dict.valueForKey("item_img_address") as! String;
                    var itemDesc = dict.valueForKey("item_short_desc") as! String;
                    var itemQuantity = dict.valueForKey("item_quantity") as! String;
                    var itemCost = dict.valueForKey("item_cost") as! Int;
                    var itemDescription = dict.valueForKey("item_description") as! String;
                    
                    var providerObj = NZPreviousOrderDetails(odrCode: itemCode, ordrname: itemName, ordrImgFlag: itemFlag, imgUrl: itemUrl, desc: itemDesc, quantity: itemQuantity, cost: itemCost, description: itemDescription);
                    self.itemArray.addObject(providerObj);
                    
                }
                //self.tblStore.reloadData();
                if(self.previousOrder.order_status == "Delivered"){
                    self.confirmArray = self.itemArray;
                }
                MBProgressHUD.hideHUDForView(self.view, animated: true);
                self.paggingScrooling();
                
            },
            failure: { (operation: AFHTTPRequestOperation!,
                error: NSError!) in
                println("Error: " + error.localizedDescription)
        })
    
    }

    @IBAction func btnBackAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    // Make Pagging Scrooling
    
    func paggingScrooling(){
        var controllerViewArray : [UIViewController] = []
        var ArrayData:NSMutableArray!;
        for (var i = 0; i < 2; i++)
        {
            var order = OrderHistoryTableViewController(objArray: itemArray, controller: self);
            if(i==0){
               order.title = "Details";
            }
            else{
               order.title =  "Confirmed";
            }
            
            controllerViewArray.append(order)
        }
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerViewArray, frame: CGRectMake(0.0, 50.0, self.view.frame.width, self.view.frame.size.height-50), pageMenuOptions: nil)
        pageMenu?.delegate = self;
        println(self.view.frame.height)
        self.view.addSubview(pageMenu!.view)
        //MBProgressHUD.showHUDAddedTo(pageMenu!.view, animated: true);

       
        
    }
    
    //CAPSPageMenu Delegate Method
    func willMoveToPage(controller: UIViewController, index: Int){
        if(index == 0){
        var shopViewController:OrderHistoryTableViewController = controller as! OrderHistoryTableViewController;
        shopViewController.reloadTable(self.itemArray);
        }
        else{
            var shopViewController:OrderHistoryTableViewController = controller as! OrderHistoryTableViewController;
            shopViewController.reloadTable(self.confirmArray);
        }
        
    }
    
    func didMoveToPage(controller: UIViewController, index: Int){
        println("didMoveToPagedidMoveToPagedidMoveToPagedidMoveToPage");
    }


    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
