//
//  EditAddressViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/8/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit


class EditAddressViewController: UIViewController,ListViewDelegate {

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    @IBOutlet var sectorViewLine: UIView!
    @IBOutlet var stateViewLine: UIView!
    @IBOutlet var lblState: UILabel!
    @IBOutlet var lblSector: UILabel!
    @IBOutlet var btnSector: UIButton!
    @IBOutlet var btnState: UIButton!
    
    var getCity:String!;
    var cityID:String = String()
    var areaCode:String = String()
    var listArray1:NSMutableArray!
    var listArray2:NSMutableArray!
    var isFromSpalsh:Bool!;
    var isSelectedList:Bool!;
    var listView:EditAddressTableViewController!;
    var controllr:UIViewController!;
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isSelectedList = false;
        lblState.text = Helper.fetchString(CURRENT_CITY) as? String;
        lblSector.text = Helper.fetchString(CURRENT_AREA) as? String;
        if(IJReachability.isConnectedToNetwork()){
            callCitysApi();
        }
        else{
            Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
        }
        listArray1 = NSMutableArray();
        listArray2 = NSMutableArray();
        // Do any additional setup after loading the view.
    }
    
    init(isNavigateFromSplash:Bool,controller:UIViewController) {
        self.isFromSpalsh=isNavigateFromSplash;
        self.controllr=controller;
        super.init(nibName: "EditAddressViewController", bundle: nil);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addListView(arr:NSMutableArray, num:Int){
        listView = EditAddressTableViewController(listDataArray: arr, controller: self, selectedViewNo: num);
        listView.delegate=self;
        self.view.addSubview(listView.view);
        if(num==1){
            listView.view.frame=CGRectMake(stateViewLine.frame.origin.x, stateViewLine.frame.origin.y+110, listView.view.frame.size.width, listView.view.frame.size.height);
        }
        else{
            listView.view.frame=CGRectMake(sectorViewLine.frame.origin.x, sectorViewLine.frame.origin.y+110, listView.view.frame.size.width, listView.view.frame.size.height);
        }
    }
    
    
    func selectedData(value:String, viewNo:Int, id:String){
        isSelectedList = true;
        println("selected Value is:\(value) and Num is:\(viewNo)");
        if(viewNo == 1){
            lblState.text = value;
            lblSector.text = "";
            cityID = id;
            Helper.saveStringInDefault(STATENAME, value: lblState.text!);
            Helper.saveStringInDefault(CURRENT_CITY, value: self.lblState.text!);
            if(IJReachability.isConnectedToNetwork()){
                callSectorsApi();
            }
            else{
                Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
            }
        }
        else{
            lblSector.text = value;
            areaCode = id;
            Helper.saveStringInDefault(SECTORNAME, value: lblSector.text!);
            Helper.saveStringInDefault(CURRENT_AREA, value: self.lblSector.text!);
            
    //  var str: AnyObject = Helper.fetchString(AREA_CODE)
        }
        btnState.enabled = true;
        btnSector.enabled = true;
        listView.view.removeFromSuperview();
        
        
        
        
        
        
       
        
    }

    
    @IBAction func clickToList1(sender: UIButton) {
        if(listArray1.count>0){
        addListView(listArray1, num: 1);
        btnSector.enabled = false;
        btnState.enabled = false;
        }
        
    }
    
    @IBAction func clickToList2(sender: AnyObject) {
        var areaAraay = Helper.fetchObjectArray(SELECTED_AREA_ARRAY);
        if(areaAraay.count>0){
        //listView.view.removeFromSuperview();
        addListView(areaAraay, num: 2);
        btnState.enabled = false;
        btnSector.enabled = false;
        }
    }
    
    
    func callCitysApi()
    {
        
    let manager = AFHTTPRequestOperationManager();
        manager.GET ("\(BASEURL)getCities", parameters: nil,success:
            
            { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in println("JSON: " + responseObject.description)
                
               let results: NSArray = (responseObject["cities"] as? NSArray)!
           
                var dict:NSDictionary = NSDictionary();
              
                for (var i=0; i<results.count;++i){
                    dict = results[i] as! NSDictionary;
                    var citiObj = NZCities(idd: dict.valueForKey("id") as! String, code: dict.valueForKey("city_code") as! String, name: dict.valueForKey("city_name") as! String);
                    self.listArray1.addObject(citiObj);
                }
                //NSUserDefaults.standardUserDefaults().removeObjectForKey(SELECTED_AREA_ARRAY);
                if(self.isFromSpalsh == true)&&(!self.isSelectedList){
                
                var dictObject:NSDictionary = NSDictionary();
                dictObject = results[0] as! NSDictionary;
                var objData = NZCities(idd: dictObject.valueForKey("id") as! String, code: dictObject.valueForKey("city_code") as! String, name: dictObject.valueForKey("city_name") as! String);
             
                    self.cityID = objData.city_code;
                    self.getCity = objData.city_name;
                    if(IJReachability.isConnectedToNetwork()){
                        self.callSectorsApi();
                    }
                    else{
                        Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
                    }
                }
                
                
            },
            
            failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in println("Error: " + error.localizedDescription) })
        
    }
     
    func callSectorsApi()
    {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true);

         listArray2 = NSMutableArray();
        let manager = AFHTTPRequestOperationManager();
        manager.GET ("\(BASEURL)getAreas/\(cityID)", parameters: nil,success:
            
            { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in println("JSON: " + responseObject.description)
                
                let areaResults: NSArray = (responseObject["areas"] as? NSArray)!
                
                var dict:NSDictionary = NSDictionary();
                
                for (var i=0; i<areaResults.count;++i){
                    dict = areaResults[i] as! NSDictionary;
                    var areaObj = NZAreas(areaId: dict.valueForKey("area_code") as! String, code: dict.valueForKey("area_code") as! String, name: dict.valueForKey("area_name") as! String, cityId: dict.valueForKey("city_code") as! String);
                    
                    self.listArray2.addObject(areaObj);
                   
                }
                 Helper.saveObjectArray(SELECTED_AREA_ARRAY, objArray: self.listArray2);
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                 if(self.isFromSpalsh == true)&&(!self.isSelectedList){
                   var dictobj = areaResults[0] as! NSDictionary;
                    
                    self.lblSector.text = dictobj.valueForKey("area_name") as? String;
                    self.lblState.text =  self.getCity;
                    self.areaCode = (dictobj.valueForKey("area_code") as? String)!;
                     Helper.saveStringInDefault(STATENAME, value: self.lblState.text!);
                     Helper.saveStringInDefault(SECTORNAME, value: self.lblSector.text!);
                    Helper.saveStringInDefault(CURRENT_CITY, value: self.lblState.text!);
                    Helper.saveStringInDefault(CURRENT_AREA, value: self.lblSector.text!);
                }
            },
            
            failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in println("Error: " + error.localizedDescription) })
        
    }
    
    
    @IBAction func clickToEnter(sender: AnyObject) {
        if(self.lblSector.text != ""){
        
        NSNotificationCenter.defaultCenter().postNotificationName(NOTIFYFORCURRENTLOC, object: nil);
       
        if(self.isFromSpalsh == true){
            Helper.saveBoolInDefaultkey(IS_FIRST_TIME, state: true);
            if(areaCode != ""){
             Helper.saveStringInDefault(AREA_CODE, value: areaCode)
            }
            var mainViewController = HomeViewController(nibName: "HomeViewController", bundle: nil)
            self.controllr.navigationController?.pushViewController(mainViewController, animated: true);
            
        }
        else{
            if(areaCode != ""){
             Helper.saveStringInDefault(AREA_CODE, value: areaCode)
            }
        }
        self.view.removeFromSuperview();
        
        }
        else{
            Helper.showAlert("Empty", message: "Please Select Area", delegate: self, cancelButtonTitle: "Ok");
        }
        
    }
    
    
    
    
    
    
    

}
