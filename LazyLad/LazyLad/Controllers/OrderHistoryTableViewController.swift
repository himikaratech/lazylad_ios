//
//  OrderHistoryTableViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/17/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class OrderHistoryTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var menuObjArray:NSMutableArray!;
    @IBOutlet var orderHistoryTableCustomCell: OrderHistoryTableCell!

    @IBOutlet weak var tblOrderDetails: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    init(objArray:NSMutableArray,controller:UIViewController) {
        menuObjArray = NSMutableArray();
        menuObjArray = objArray;
//        viewController = controller;
        super.init(nibName: "OrderHistoryTableViewController", bundle: nil);
    }
    
    func reloadTable(arrData:NSMutableArray){
        menuObjArray = NSMutableArray(array: arrData);
        println("MenuItem Count is:\(menuObjArray.count)")
         //self.tblOrderDetails.reloadData();
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //***** TableView Delegates *****//
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menuObjArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        NSBundle.mainBundle().loadNibNamed("OrderHistoryTableCell", owner: self, options: nil);
        var cell:OrderHistoryTableCell=orderHistoryTableCustomCell;
        var oredreDetails:NZPreviousOrderDetails = menuObjArray.objectAtIndex(indexPath.row) as! NZPreviousOrderDetails;
        cell.bindWithOrderDetailsObj(oredreDetails);

        return cell
    }
    
    

}
