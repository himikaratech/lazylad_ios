//
//  ConfirmOrderViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/5/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class ConfirmOrderViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var lblDiscountLabel: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblOrderAmount: UILabel!
    @IBOutlet weak var lblDeliveryCharges: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblTotalCharges: UILabel!
    @IBOutlet weak var vwCoupon: UIView!
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var scrlview: UIScrollView!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var vwApply: UIView!
    @IBOutlet weak var txtCoupon: UITextField!
    var coupontypeid:String!;
    var couponamount: String!;
    var minimumorderamount: String!
    var totalAmount:String!;
    var totalSelectedItemObjectArray:NSMutableArray!;
     var jsonDataArray:NSMutableArray!;
    override func viewDidLoad() {
        super.viewDidLoad()
        lblDiscount.hidden = true;
        lblDiscountLabel.hidden = true;
        jsonDataArray = NSMutableArray();
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        self.vwApply.hidden = true;
        self.vwCoupon.hidden = false;
        totalSelectedItemObjectArray = NSMutableArray();
        ShowSelectedItemPricesData();

        // Do any additional setup after loading the view.
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        var contentInsets:UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardFrame.height, right: 0.0);
        
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.scrlview.contentInset = contentInsets;
            self.scrlview.contentSize = CGSizeMake(self.scrlview.frame.size.width, self.scrlview.frame.height-keyboardFrame.height)
        })
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        var contentInsets:UIEdgeInsets = UIEdgeInsetsZero;
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.scrlview.contentInset = contentInsets;
            self.scrlview.contentSize = CGSizeMake(self.scrlview.frame.size.width, self.scrlview.frame.height+keyboardFrame.height)
        })
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil);
    }
    
    @IBAction func clickToCancel(sender: AnyObject) {
        self.vwApply.hidden = true;
        self.vwCoupon.hidden = false;
    }

    @IBAction func clickToApply(sender: AnyObject) {
        if(!txtCoupon.text.isEmpty){
            if(IJReachability.isConnectedToNetwork()){
                callcheckCouponValidityFinalApi();
            }
            else{
                Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
            }
        }
        else{
            Helper.showAlert("Empty Text", message: "Please Enter Coupon Code", delegate: self, cancelButtonTitle: "OK");
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func clickToUseCoupon(sender: AnyObject) {
        self.vwCoupon.hidden = true;
        self.vwApply.hidden = false;
        self.txtCoupon.becomeFirstResponder();
    }
    @IBAction func clickToViewItemInCart(sender: AnyObject) {
        var shopingCart = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
        self.navigationController?.pushViewController(shopingCart, animated: true);
    }
    
    @IBAction func clickToPlaceOrder(sender: AnyObject) {
        if(IJReachability.isConnectedToNetwork()){
            postOrderServiceApi();
        }
        else{
            Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
        }
        
        
    }
    @IBAction func clickToBAck(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    
    func ShowSelectedItemPricesData(){
        var totalItemPrice:Int = 0;
        var itemDict = NSMutableDictionary();
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            itemDict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
            
            for (key,value) in itemDict {
                var itemArray = itemDict.valueForKey("\(key)") as! NSMutableArray;
                     var dict = ["code":"\(key)","itemQuantitySelected":"\(itemArray.count)"];
                    jsonDataArray.addObject(dict);
                
               
                
                totalSelectedItemObjectArray.addObjectsFromArray(itemArray as [AnyObject]);
                for(var i=0 ; i<itemArray.count;++i ){
                    var itemObj = itemArray.objectAtIndex(i) as! NZItemsServiceProviderCategoryWise;
                    totalItemPrice += itemObj.is_item_cost;
                }
                
            }
            
            self.lblOrderAmount.text = "₹ \(totalItemPrice)"
            
           
            totalAmount = "\(totalItemPrice)";
            var delivery_charge:Int = 0;
            var sellerMinOrder:String = Helper.fetchString(SELLER_MIN_ORDER) as! String;
            if(totalAmount.toInt() < sellerMinOrder.toInt()){
                self.lblDeliveryCharges.text = "₹ 20";
                totalItemPrice = totalItemPrice + 20;
            }
            else{
                self.lblDeliveryCharges.text = "₹ 0";
                 totalItemPrice = totalItemPrice + 0;
            }
           
            totalAmount = "\(totalItemPrice)";
             self.lblTotalCharges.text = "₹ \(totalItemPrice)"
            
            
        }

    }
    
    // ********************* CheckCouponValidityFinal API ************************
    
    func callcheckCouponValidityFinalApi(){
        MBProgressHUD.showHUDAddedTo(self.view, animated: true);

        let manager = AFHTTPRequestOperationManager();
        var params = ["coupon_code":txtCoupon.text]
        
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.requestSerializer = AFHTTPRequestSerializer();
        manager.POST("\(BASEURL)checkCouponValidityFinal", parameters: params, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
            println("Yes thies was a success")
            var error: NSError?
            let jsonData: NSData = responseObject as! NSData;
            let jsonDict = NSJSONSerialization.JSONObjectWithData(jsonData, options: nil, error: &error)as! NSDictionary
            println("JSON:\(jsonDict)")
            
            var code:Int = jsonDict["error"] as! Int;
            if(code == 1){
            self.coupontypeid = jsonDict["coupon_type_id"] as! String;
            self.couponamount = jsonDict["coupon_amount"] as! String;
            self.minimumorderamount = jsonDict["minimum_order_amount"] as! String;
                if(self.totalAmount.toInt()>self.minimumorderamount.toInt()){
                
                var total:Int = 0;
                if(self.couponamount == nil){
                    self.couponamount = "0";
                }
                else{
                    total = self.totalAmount.toInt()!-self.couponamount.toInt()!;
                    self.lblTotalCharges.text = "₹ \(total)";
                    self.lblDiscount.hidden = false;
                    self.lblDiscountLabel.hidden = false;
                    self.lblDiscount.text = "₹ \(self.couponamount)"
                    self.txtCoupon.text = "";
                    self.vwApply.hidden = true;
                    self.vwCoupon.hidden = false;
                    
                }
                }
                else{
                    Helper.showAlert("Alert", message: "Your Minimum should be ₹ \( self.minimumorderamount)", delegate: self, cancelButtonTitle: "Ok");
                }
            }
            else{
                Helper.showAlert("Invalid coupon code", message: "Please use correct coupon code", delegate: self, cancelButtonTitle: "OK");
                self.txtCoupon.text = "";

            }
            
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            },
            
            failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                println("We got an error here.. \(error.localizedDescription)")
        })
        
    }
    
    // ********************* postOrderToServerFinal API ************************
    
    func postOrderServiceApi(){
        var spCode:String = Helper.fetchString(SP_CODE) as! String;
        var stCode:String = Helper.fetchString(ST_CODE) as! String;
        var userID:String = Helper.fetchString(USER_ID) as! String;
        var userAddressCode:String = Helper.fetchString(USER_ADDRESS_CODE) as! String;
        
        let dateFormatter = NSDateFormatter()
        let date = NSDate();
        dateFormatter.dateFormat = "yyyy-MM-dd";
        let dateStr = dateFormatter.stringFromDate(date);
        
        
        // Set  Total Ammount From Coupon
        var tot_ItemAmt:Int = 0;
        if(self.couponamount == nil){
            self.couponamount = "0";
            tot_ItemAmt = totalAmount.toInt()!;
        }
        else{
            tot_ItemAmt = totalAmount.toInt()!-self.couponamount.toInt()!;
        }
        
        // Set Total Amount from SELLER_MIN_ORDER
        var delivery_charge:Int = 0;
        var sellerMinOrder:String = Helper.fetchString(SELLER_MIN_ORDER) as! String;
        if(totalAmount.toInt() < sellerMinOrder.toInt()){
            delivery_charge = 20;
        }
        

        
        
       // Convert Array To Json String Here
        let theJSONData = NSJSONSerialization.dataWithJSONObject(
            jsonDataArray ,
            options: NSJSONWritingOptions(0),
            error: nil)
        let theJSONText = NSString(data: theJSONData!,
            encoding: NSASCIIStringEncoding)
        println("JSON string = \(theJSONText!)")
    
        let manager = AFHTTPRequestOperationManager();
        var params = ["sp_code":spCode,"st_code":stCode,"user_id":userID,"user_exp_del_time":dateStr,"user_address_code":userAddressCode,"tot_amount":totalAmount.toInt()!-self.couponamount.toInt()!,"delivery_charges":"\(delivery_charge)","discount_amount":self.couponamount,"total_items_amount":totalAmount.toInt()!-self.couponamount.toInt()!,"JsonDataArray":theJSONText!];
        
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.requestSerializer = AFHTTPRequestSerializer();
        manager.POST("\(BASEURL)postOrderToServerFinal", parameters: params, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
            println("Yes thies was a success")
            var error: NSError?
            let jsonData: NSData = responseObject as! NSData;
            let jsonDict = NSJSONSerialization.JSONObjectWithData(jsonData, options: nil, error: &error)as! NSDictionary
            println("JSON:\(jsonDict)")
            var orderCode:Int = jsonDict.valueForKey("order_code") as! Int;
            Helper.saveStringInDefault(ORDER_CODE, value: "\(orderCode)");
            var placeView = PlaceOrderViewController(nibName: "PlaceOrderViewController", bundle: nil)
            self.navigationController?.pushViewController(placeView, animated: true);
        
            },
            
            failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                println("We got an error here.. \(error.localizedDescription)")
        })
        
    }
    



    
    // Text Field Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder();
        return true;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
