//
//  AddressListViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class AddressListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var lblPleaseAddAddress: UILabel!
    @IBOutlet weak var btnTopNav: UIButton!

    @IBOutlet weak var tblAddress: UITableView!
    var  addressObjectArray:NSMutableArray!;
    var menuView:MenuViewController!;
    @IBOutlet var addressCustomCell: AddressListCell!
    var isComeFromCheckOut:Bool!;
    override func viewDidLoad() {
        super.viewDidLoad()
         self.btnTopNav.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
        gestuer();
        addressObjectArray = NSMutableArray();
        addMenuView();
        // Do any additional setup after loading the view.
    }
    
    
    // Gesture Implement Here
    func gestuer(){
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                self.navMenuShow();
            case UISwipeGestureRecognizerDirection.Left:
                self.navMenuHide();
            default:
                break
            }
        }
    }

    
    
    // Add Menu View As a Subview
    func addMenuView(){
        menuView = MenuViewController(title: "Home", controller: self);
        self.view.addSubview(menuView.view);
        menuView.view.frame = CGRectMake(-320, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);
    }
    
    override func viewDidAppear(animated: Bool)
    {
        addressObjectArray = Helper.fetchObjectArray(SAVE_ADDRESS_OBJECT);
        self.tblAddress.reloadData();
        if(self.addressObjectArray.count>0)
        {
            self.lblPleaseAddAddress.hidden = true;
        }
        else
        {
            self.lblPleaseAddAddress.hidden = false;
        }

    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        
    }
    @IBAction func slideMenuBtnAction(sender: UIButton) {
        sender.selected = !sender.selected;
         self.navMenuShow()
//        if(!sender.selected){
//            self.navMenuHide()
//        }
//        else{
//            self.navMenuShow()
//        }
    }
    
    func navMenuHide()
    {
        UIView.animateWithDuration(0.5, animations: {
            self.menuView.view.frame = CGRectMake(-320, 0, self.menuView.view.frame.size.width, self.menuView.view.frame.size.height);
            self.btnTopNav.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
    
        })
    }
    func navMenuShow()
    {
        UIView.animateWithDuration(0.5, animations: {
            self.menuView.view.frame = CGRectMake(0, 0, self.menuView.view.frame.size.width, self.menuView.view.frame.size.height);
             self.btnTopNav.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
            
        })
        
    }

    @IBAction func clickToAdd(sender: AnyObject) {
        var addAddressView = AddAddressViewController(nibName: "AddAddressViewController", bundle: nil);
        self.navigationController?.pushViewController(addAddressView, animated: true);
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //***** TableView Delegates *****//
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       
        return addressObjectArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        NSBundle.mainBundle().loadNibNamed("AddressListCell", owner: self, options: nil);
        var cell:AddressListCell=addressCustomCell;
        cell.selectionStyle = UITableViewCellSelectionStyle.None;
        cell.lblSerialNo.text = String(indexPath.row + 1);
        cell.bindAddresObject(addressObjectArray.objectAtIndex(indexPath.row) as! AddressUserPreference);
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(isComeFromCheckOut == true){
            var objData:AddressUserPreference = addressObjectArray.objectAtIndex(indexPath.row) as! AddressUserPreference;
            Helper.saveStringInDefault(USER_ADDRESS_CODE, value: objData.userCodeAddress);
        var confirmView = ConfirmOrderViewController(nibName: "ConfirmOrderViewController", bundle: nil);
        self.navigationController?.pushViewController(confirmView, animated: true);
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
