//
//  Config.h
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

#ifndef LazyLad_Config_h
#define LazyLad_Config_h
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <MBProgressHUD.h>

#define SAVE_ADDRESS_OBJECT   "saveAddressObject"
#define AREA_CODE             "area_code"
#define STATENAME             "statename"
#define SECTORNAME            "sectorname"
#define IS_FIRST_TIME         "isFirstTime"
#define USER_CODE             "usercode"
#define CURRENT_CITY          "current_city"
#define CURRENT_AREA          "current_area"
#define SP_CODE               "sp_code"
#define ST_CODE               "st_code"
#define USER_ID               "user_id"
#define USER_ADDRESS_CODE     "user_address_code"
#define SELLER_MIN_ORDER      "seller_min_order"
#define ORDER_CODE            "order_code"
#define SELECTED_ITEM_KEY     "selectedItemKey"
#define SELECTED_AREA_ARRAY    "selectedAreaArray"
#define NOTIFYFORCURRENTLOC   "CurrentLocationNotification"
#define BASEURL               "http://www.angulartechnologies.com/task_manager/v1/"
//#define BASEURL               "http://52.74.211.202/task_manager/v1/"
#define HOME_IMAGE_URL_THUMB            "http://angulartechnologies.com/service_type_images/thumb/"
#define HOME_IMAGE_URL_FULLIMAGES       "http://angulartechnologies.com/service_type_images/"
#define HOME_IMAGE_URL_FLOWER_THUMB     "http://angulartechnologies.com/service_type_images/thumb/7"
#define HOME_IMAGE_URL_FLOWER_FULL      "http://angulartechnologies.com/service_type_images/7"

#endif
