//
//  AppDelegate.swift
//  LazyLad
//
//  Created by Lazylad on 6/3/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var user_Udid:String!
    var addAddressArray:NSMutableArray!
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        if(Helper.fetchBool(IS_FIRST_TIME)){
            showHomeScreen();
        }
        else{
            let UUID = CFUUIDCreateString(nil, CFUUIDCreate(nil));
            user_Udid = UUID as String;
           Helper.saveStringInDefault(CURRENT_CITY, value: "");
           Helper.saveStringInDefault(CURRENT_AREA, value: "");
            addAddressApi();
            showSplashScreen();
        }
        return true;
    }
    
    func showSplashScreen(){
        var splahView = SplashViewController(nibName: "SplashViewController", bundle: nil)
        var navigationController = UINavigationController(rootViewController: splahView)
        navigationController.setNavigationBarHidden(true, animated: false);
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
    
    func showHomeScreen(){
        var mainViewController = HomeViewController(nibName: "HomeViewController", bundle: nil)
        var navigationController = UINavigationController(rootViewController: mainViewController)
        navigationController.setNavigationBarHidden(true, animated: false);
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }

    // ********************* addUser API ************************
    func addAddressApi(){
        //var getUserId:Int = 0;
        addAddressArray = NSMutableArray();
        let manager = AFHTTPRequestOperationManager();
        var params = ["usercode":"0",
                      "reg_id": user_Udid]
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.requestSerializer = AFHTTPRequestSerializer();
        manager.POST("\(BASEURL)addNewUser", parameters: params, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
            println("Yes thies was a success")
            var error: NSError?
            let jsonData: NSData = responseObject as! NSData;
            let jsonDict = NSJSONSerialization.JSONObjectWithData(jsonData, options: nil, error: &error)as! NSDictionary
            println("JSON:\(jsonDict)")
             let results: NSArray = (jsonDict["user_details"] as? NSArray)!
             var dict:NSDictionary = NSDictionary();
            dict = results[0] as! NSDictionary;
            var getUserCode = dict.valueForKey("user_code") as! Int;
            Helper.saveStringInDefault(USER_ID, value: "\(getUserCode)");
            },
            
            failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                println("We got an error here.. \(error.localizedDescription)")
        })
        
    }

    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

