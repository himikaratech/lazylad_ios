//
//  Helper.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class Helper: NSObject {
   
    //DISABLE ALL TOUCH
    class func disableAllEventTouch(){
        UIApplication.sharedApplication().beginIgnoringInteractionEvents();
    }
    //ENABLE ALL TOUCH
    class func enableAllEventTouch(){
        UIApplication.sharedApplication().endIgnoringInteractionEvents();
    }
    
    //OPEN URL IN SAFARI
    class func OpenSafariWithURl(urlStr:String){
        UIApplication.sharedApplication().openURL(NSURL(string: urlStr)!);
    }
    
    //SAVE BOOL IN DEFAULT
    class func saveBoolInDefaultkey(key:String,state:Bool) {
        NSUserDefaults.standardUserDefaults().setBool(state, forKey: key);
        NSUserDefaults.standardUserDefaults().synchronize();
    }
    
    //FETCH BOOL FROM DEFAULT
    class func fetchBool(key:String)->Bool{
        return NSUserDefaults.standardUserDefaults().boolForKey(key);
    }
    
    //SAVE STRING IN USER DEFAULT
    class func saveStringInDefault(key:String,value:String) {
        NSUserDefaults.standardUserDefaults().setObject(value, forKey: key);
        NSUserDefaults.standardUserDefaults().synchronize();
    }
    
    //FETCH STRING FROM USER DEFAULT
    class func fetchString(key:String)->AnyObject {
        return NSUserDefaults.standardUserDefaults().valueForKey(key)!;
    }
    
    
    // SAVE OBJECT ARRAY IN USER DEFAULT
    class func saveObjectArray(key:String, objArray:NSMutableArray){
        let data =  NSKeyedArchiver.archivedDataWithRootObject(objArray);
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize();
    
    }
    
    // SAVE OBJECT DICTONARY IN USER DEFAULT
    class func saveObjectDict(key:String, objArray:NSMutableDictionary){
        let data =  NSKeyedArchiver.archivedDataWithRootObject(objArray);
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize();
        
    }

    
    // FETCH OBJECT ARRAY FROM USER DEFAULT
    class func fetchObjectArray(key:String)->NSMutableArray {
        var objArray:NSMutableArray=NSMutableArray();
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(key) as? NSData {
             objArray = (NSKeyedUnarchiver.unarchiveObjectWithData(data) as? NSMutableArray)!
           
        }
        return objArray;
    }
    
    // FETCH OBJECT DICTIONARY FROM USER DEFAULT
    class func fetchObjectDict(key:String)->NSMutableDictionary {
        var objArray:NSMutableDictionary=NSMutableDictionary();
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(key) as? NSData {
            objArray = (NSKeyedUnarchiver.unarchiveObjectWithData(data) as? NSMutableDictionary)!
            
        }
        return objArray;
    }
    
    //REMOVE NSUSER DEFAULT
    class func removeUserDefault(key:String) {
        NSUserDefaults.standardUserDefaults().removeObjectForKey(key);
    }
    
    
    //SHOW ALERT VIEW
    class func showAlert(title:String,message:String,var delegate:UIViewController,cancelButtonTitle:String) -> Void {
        var version:NSString = UIDevice.currentDevice().systemVersion as NSString;
        if  version.doubleValue >= 8 {
            var alert = UIAlertController(title:title, message:message, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title:cancelButtonTitle, style: UIAlertActionStyle.Default, handler: nil))
            var rootViewController = UIApplication.sharedApplication().keyWindow?.rootViewController;
            delegate = rootViewController!;
            delegate.presentViewController(alert, animated: true, completion: nil);
            
        }
        else {
            var alert = UIAlertView(title:title,message:message,delegate:delegate,cancelButtonTitle:cancelButtonTitle);
            alert.show();
        }
        
    }

   // SEARCH DATA WITH PREDICATE
    class func getSearchList(searchText:String,searchList:NSArray,searchObject:String) -> NSArray {
        //var predicateFilter = NSPredicate(format:"SELF.%@ beginswith[c] %@",searchObject,searchText)
        var predicateFilter = NSPredicate(format:"SELF.%@ CONTAINS[c] %@",searchObject,searchText)
        var filteredList = searchList.filteredArrayUsingPredicate(predicateFilter)
        return filteredList;
    }
    
    class func pushController(cntrl:UIViewController, screenName:String) {
        if let viewController = NSClassFromString(screenName) as? UIViewController.Type {
           
        
    }
    
    
    
    }
    
    
    
}
