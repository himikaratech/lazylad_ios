//
//  Extension.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

extension UITextField{
    func numberKeybord(){
        let numberToolbar=UIToolbar();
        numberToolbar.frame=CGRectMake(0, 0, 320, 50);
        numberToolbar.barStyle=UIBarStyle.BlackTranslucent;
        var array:NSMutableArray=NSMutableArray();
        var cancelBtn = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Bordered, target: self, action: Selector("cancelNumberPad"));
        array.addObject(cancelBtn);
        var space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil);
        array.addObject(space);
        var doneBtn = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: Selector("doneNumberPad"));
        array.addObject(doneBtn)
        numberToolbar.items=array as [AnyObject];
        numberToolbar.sizeToFit();
        self.inputAccessoryView=numberToolbar;
        
    }
    //******* CancelNumberPad **********
    func cancelNumberPad(){
        self.resignFirstResponder();
        self.text="";
    }
    
    //******* DoneNumberPad **********
    func doneNumberPad(){
        self.resignFirstResponder();
    }
    
    
    //******* Is Blank ************
    
    func isBlank()->Bool{
        if(self.text==nil){
            return true
        }
        else{
            return false;
        }
    }
    
    //******* Set Padding *************
    
    func setPadding(){
        var paddingView=UIView(frame: CGRectMake(0, 0, 8, 20));
        self.leftView=paddingView;
        self.leftViewMode=UITextFieldViewMode.Always;
        
        var paddingViewRight=UIView(frame: CGRectMake(0, 0, 8, 20));
        self.rightView=paddingViewRight;
        self.rightViewMode=UITextFieldViewMode.Always;
        
    }
    
    
//    @interface UIColor (Extensions)
//    - (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha;
//    @end
    
    
    
}
/*
@implementation UIColor (Extensions)
- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    // Convert hex string to an integer
    unsigned int hexint = [self intFromHexString:hexStr];
    
    // Create color object, specifying alpha as well
    UIColor *color =
        [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
            green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
            blue:((CGFloat) (hexint & 0xFF))/255
            alpha:alpha];
    
    return color;
    }
    
    //Function For Hex Color Use
    - (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}

@end
*/

extension UIColor{
    func getUIColorWithHexString(colorCode: String, alpha:CGFloat)->UIColor{
        var scanner = NSScanner(string:colorCode)
        var color:UInt32 = 0;
        scanner.scanHexInt(&color)
        
        let mask = 0x000000FF
        let r = CGFloat(Float(Int(color >> 16) & mask)/255.0)
        let g = CGFloat(Float(Int(color >> 8) & mask)/255.0)
        let b = CGFloat(Float(Int(color) & mask)/255.0)
        var clr:UIColor;
        clr = UIColor(red: r, green: g, blue: b, alpha: alpha);
        return clr;
}
}

//Random Color Generate.
extension UIColor {
    static func randomColor() -> UIColor {
        let r = randomCGFloat()
        let g = randomCGFloat()
        let b = randomCGFloat()
        
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
}

func randomCGFloat() -> CGFloat {
    return CGFloat(arc4random()) / CGFloat(UInt32.max)
}
