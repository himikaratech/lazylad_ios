//
//  AddressUserPreference.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class AddressUserPreference: NSObject, NSCoding {
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    var name:String!;
    var phoneNumber:String!;
    var houseNumber:String!;
    var subArea:String!;
    var areaCity:String!;
    var userCode:String!;
    var userCodeAddress:String!;
    
    // Init Method For Address
    init(nme:String, phNo:String, houseNo:String, subLocality:String,city:String, usercode:String, usercodeAdrs:String) {
        name = nme;
        phoneNumber = phNo;
        houseNumber = houseNo;
        subArea = subLocality;
        areaCity = city;
        userCode = usercode;
        userCodeAddress = usercodeAdrs;
    }
    
    
    required init(coder decoder: NSCoder) {
        self.name = decoder.decodeObjectForKey("name") as! String?
        self.phoneNumber = decoder.decodeObjectForKey("phoneNumber") as! String?
        self.houseNumber = decoder.decodeObjectForKey("houseNumber")as! String?
        self.subArea = decoder.decodeObjectForKey("subArea") as! String?
        self.areaCity = decoder.decodeObjectForKey("areaCity") as! String?
        self.userCode = decoder.decodeObjectForKey("usercode") as! String?
        self.userCodeAddress = decoder.decodeObjectForKey("usercodeaddress") as! String?
        
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.name, forKey: "name");
        coder.encodeObject(self.phoneNumber, forKey: "phoneNumber");
        coder.encodeObject(self.houseNumber, forKey: "houseNumber");
        coder.encodeObject(self.subArea, forKey: "subArea");
        coder.encodeObject(self.areaCity, forKey: "areaCity");
        coder.encodeObject(self.userCode, forKey: "usercode");
        coder.encodeObject(self.userCodeAddress, forKey: "usercodeaddress");
      
        
    }
    
    
}
