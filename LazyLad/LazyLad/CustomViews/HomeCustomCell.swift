//
//  HomeCustomCell.swift
//  LazyLad
//
//  Created by Lazylad on 6/3/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class HomeCustomCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var imgIcon: UIImageView!

    @IBOutlet weak var lblText: UILabel!
}
