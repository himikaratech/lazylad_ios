//
//  AddressListCell.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class AddressListCell: UITableViewCell {
    @IBOutlet weak var vwBgNo: UIView!

    @IBOutlet weak var lblSerialNo: UILabel!
  
    @IBOutlet weak var lblName: UILabel!

    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMobNo: UILabel!
    
    func bindAddresObject(obj: AddressUserPreference){
        self.vwBgNo.backgroundColor = UIColor.randomColor();
        lblName.text = obj.name;
        lblMobNo.text = obj.phoneNumber;
        lblAddress.text = obj.houseNumber + ", " + obj.subArea + " , " + obj.areaCity;
    }
    
}
