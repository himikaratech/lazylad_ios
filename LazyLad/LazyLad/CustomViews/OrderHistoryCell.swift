//
//  OrderHistoryCell.swift
//  LazyLad
//
//  Created by NZT on 6/8/15.
//  Copyright (c) 2015 Nzt. All rights reserved.
//

import UIKit
protocol orderHistoryDelegate{
    func tableReloadFromCancel();
    func clickToBillView(ordrCode:String);
}


class OrderHistoryCell: UITableViewCell {
    @IBOutlet weak var imgProgressStatus: UIImageView!
    @IBOutlet weak var bgcolor: UIView!

    @IBOutlet weak var btnCancelView: UIButton!
    @IBOutlet var lblCancelView: UILabel!
    @IBOutlet var lblOrderNumber: UILabel!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var lblStoreName: UILabel!
    @IBOutlet var lblOrderStatus: UILabel!
    var orderObject:NZPreviousOrder!;
    
    var delegate:orderHistoryDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindDataOrderHistoryObjet(objData:NZPreviousOrder){
        orderObject = objData;
        self.bgcolor.backgroundColor = UIColor.randomColor();
        self.lblStoreName.text = objData.sp_name as String;
        self.lblOrderStatus.text = objData.order_status;
        self.lblTotalAmount.text = "₹ \(objData.order_amount)";
        self.lblOrderNumber.text = "\(objData.order_code)";
        if(objData.order_status == "Pending")
        {
         // Code for Pending Status
            self.imgProgressStatus.image = UIImage(named: "onegreen-1");
            self.btnCancelView.tag = 501;
            self.btnCancelView.hidden = true;
            self.lblCancelView.hidden = true;
            self.btnCancelView.setTitle("Cancel", forState: UIControlState.Normal);
            self.lblCancelView.text = "Cancel";
            
        }
        else if(objData.order_status == "Confirmed"){
            // Code for Delivered Status
            self.imgProgressStatus.image = UIImage(named: "twogreen-check-1");
             self.btnCancelView.hidden = false;
             self.lblCancelView.hidden = false;
            self.btnCancelView.tag = 502;
            self.btnCancelView.setTitle("View Bill", forState: UIControlState.Normal);
             self.lblCancelView.text = "View Bill";
            
        }
        else{
            // Code for Cancel Status
            self.imgProgressStatus.image = UIImage(named: "green-check-1");
            self.btnCancelView.hidden = false;
            self.lblCancelView.hidden = false;
            self.btnCancelView.tag = 502;
            self.btnCancelView.setTitle("View Bill", forState: UIControlState.Normal);
            self.lblCancelView.text = "View Bill";
        }
    }
    
    
    @IBAction func clickToCancelOrder(sender: UIButton) {
        if(sender.tag == 501){
           // Cancel Button Functionality
            if(IJReachability.isConnectedToNetwork()){
                //cancelUsersPreviousOrder();
            }
            else{
            //Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
            Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: OrderHistoryViewController() , cancelButtonTitle: "Ok")
            }

        }
        if(sender.tag == 502){
            // Show Bill Screen for this Item
            self.delegate.clickToBillView(orderObject.order_code);
            
        }
       
    }

    
    
        func cancelUsersPreviousOrder(){
            MBProgressHUD.showHUDAddedTo(self, animated: true);
    
        //    addressArray = NSMutableArray();
            let manager = AFHTTPRequestOperationManager();
            var params = ["order_code":orderObject.order_code as String]
    
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer();
            manager.POST("\(BASEURL)cancelUsersPreviousOrder", parameters: params, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                println("Yes thies was a success")
                var error: NSError?
                let jsonData: NSData = responseObject as! NSData;
                let jsonDict = NSJSONSerialization.JSONObjectWithData(jsonData, options: nil, error: &error)as! NSDictionary
                println("JSON:\(jsonDict)")
                
                var errorCode = jsonDict.valueForKey("error") as! Int
                if (errorCode == 1){
                    self.lblOrderStatus.text = "Cancelled"
                    self.orderObject.order_status = "Cancelled"
                    self.delegate.tableReloadFromCancel()
                    
                }
                else{
                self.lblOrderStatus.text = ""
                self.delegate.tableReloadFromCancel()

                }
                MBProgressHUD.hideHUDForView(self, animated: true)

                },
    
                failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                    println("We got an error here.. \(error.localizedDescription)")
            })
            
        }

    
}




/*if(IJReachability.isConnectedToNetwork()){
callServiceProviderApi();
}
else{
Helper.showAlert("Failed", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "Ok");
}
*/
//                MBProgressHUD.hideHUDForView(self.view, animated: true)
