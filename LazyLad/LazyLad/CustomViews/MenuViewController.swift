//
//  MenuViewController.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit
import Social
import MessageUI

class MenuViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate{
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBOutlet weak var lblAddress: UILabel!
    var itemsArray:NSArray!;
    var itemImagesArray:NSArray!
    var screenName:String?;
    var viewController:UIViewController!
    var addressView:EditAddressViewController!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblAddress.text = "\(Helper.fetchString(SECTORNAME)), \(Helper.fetchString(STATENAME))";
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("Notify"), name: NOTIFYFORCURRENTLOC, object: nil);
        itemsArray = NSArray(arrayLiteral: "Home", "Addresses", "Order History", "Rate Us", "Feedback", "Share", "Call Us");
        itemImagesArray = NSArray(arrayLiteral: "home-icon", "address-icon", "order-history-icon", "rateus-iocn", "feedback-icon", "share-icon","call-icon");

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    init(title:String,controller:UIViewController) {
        self.screenName=title;
        self.viewController=controller;
        super.init(nibName: "MenuViewController", bundle: nil);
    }

    //Notificatiopn Fired
    func Notify(){
        self.lblAddress.text = "\(Helper.fetchString(SECTORNAME)) \(Helper.fetchString(STATENAME))";
    }
    
    
    //***** TableView Delegates *****//
    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
//    {
//        var imageView : UIImageView
//        imageView  = UIImageView(frame:CGRectMake(10, 50, 100, 160));
//        imageView.image = UIImage(named:"navigation-bg-img")
//        
//        return imageView
//    }
    

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return itemsArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        
        var cell = tableView.dequeueReusableCellWithIdentifier("CELL") as? UITableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "CELL")
        }
        
        
        var image: AnyObject = itemImagesArray[indexPath.row]
        cell!.imageView?.image = UIImage(named:image as! String)
        
        
        cell!.textLabel?.text  = itemsArray[indexPath.row] as? String
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        switch(indexPath.row)
        {
        case 0:
            var homeView = HomeViewController(nibName: "HomeViewController", bundle: nil);
            viewController.navigationController?.pushViewController(homeView, animated: true);
            
        case 1:
            var addressView = AddressListViewController(nibName: "AddressListViewController", bundle: nil);
            addressView.isComeFromCheckOut = false;
            viewController.navigationController?.pushViewController(addressView, animated: true);
            
            
        case 2:
            var addressView = OrderHistoryViewController(nibName: "OrderHistoryViewController", bundle: nil);
            viewController.navigationController?.pushViewController(addressView, animated: true);

            println("selected %@",indexPath.row)

        case 3:
            UIApplication.sharedApplication().openURL(NSURL(string: "itms://itunes.apple.com/app/lazylad/id1019080985?mt=8&uo=4")!)
            println("selected %@",indexPath.row)
            
        case 4:
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
            println("selected %@",indexPath.row)
            
        case 5:
            let activityViewController = UIActivityViewController(
                activityItems: ["Try this app,its great - Free your evenings and weekends of daily needs shooping with LazyLad. Download it from AppStore "],
                applicationActivities: nil)
            
            presentViewController(activityViewController, animated: true, completion: nil)
            println("selected %@",indexPath.row)
            
        case 6:
            println("selected %@",indexPath.row)
            UIApplication.sharedApplication().openURL(NSURL(string:"tel:8011139511")!)
            
           default:
            println("selected nothing")
        }
        
        
    }

    @IBAction func clickToAddress(sender: AnyObject) {
        addressView =  EditAddressViewController(isNavigateFromSplash: false, controller: self);var mainViewController = HomeViewController(nibName: "HomeViewController", bundle: nil)
        self.view.addSubview(addressView.view);
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    ////******Feedback******////
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["contact@lazylad.com"])
        mailComposerVC.setSubject("Customer Feedback")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    
    

}
