//
//  CheckOutCell.swift
//  LazyLad
//
//  Created by Lazylad on 6/5/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

protocol CheckOutCellDelegate{
    func tableReload();
}
class CheckOutCell: UITableViewCell {
    
    @IBOutlet weak var lblStrike: UILabel!
    @IBOutlet weak var imgItem: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblRateF: UILabel!
    
    @IBOutlet weak var lblItemNo: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    var delegate: CheckOutCellDelegate?
    @IBOutlet weak var btnSubtract: UIButton!
    var itemObj:NZItemsServiceProviderCategoryWise!;
    
    func bindDataWithitemsServiceProvider(itemObject:NZItemsServiceProviderCategoryWise, itemNum:Int){
        itemObj = itemObject;
        self.lblName.text = itemObject.is_item_name;
        self.lblType.text = itemObject.is_item_desc;
        if(itemObject.is_item_mrp > itemObject.is_item_cost){
            self.lblRate.hidden = false;
            self.lblStrike.hidden = false;
        }
        else{
            self.lblRate.hidden = true;
            self.lblStrike.hidden = true;
        }
        self.lblRate.text = "₹ \(toString(itemObject.is_item_mrp))";
        self.lblRateF.text = "₹ \(toString(itemObject.is_item_cost))";
        self.imgItem.sd_setImageWithURL(NSURL(string: itemObject.is_item_img_address), placeholderImage: UIImage(named: ""));
        var itemDict = NSMutableDictionary();
        var itemArray = NSMutableArray();
        //        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
        //            itemDict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
        //            if(itemDict.valueForKey(itemObj.is_item_code)?.count>0){
        //                itemArray = itemDict.valueForKey(itemObj.is_item_code) as! NSMutableArray;
        //                itemCount(itemArray.count);
        //            }
        //
        //        }
        
        itemCount(itemNum);
    }
    
    
    @IBAction func clickToAdd(sender: AnyObject) {
        //Save Items Array into Dict
        var itemDict = NSMutableDictionary();
        var itemAddArray = NSMutableArray();
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            itemDict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
            if(itemDict.valueForKey(itemObj.is_item_code)?.count>0){
                itemAddArray = itemDict.valueForKey(itemObj.is_item_code) as! NSMutableArray;
            }
            
        }
        itemAddArray.addObject(itemObj);
        itemDict .setObject(itemAddArray, forKey: itemObj.is_item_code);
        Helper.saveObjectDict(SELECTED_ITEM_KEY, objArray: itemDict);
        itemCount(itemAddArray.count);
        NSNotificationCenter.defaultCenter().postNotificationName("SELECTED_ITEM_NOTIFY", object: nil);
    }
    @IBAction func clickToSubtract(sender: AnyObject) {
        
        // Remove Item From Dict
        var itemRemoveDict = NSMutableDictionary();
        var itemRemoveArray = NSMutableArray();
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            itemRemoveDict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
            if(itemRemoveDict.valueForKey(itemObj.is_item_code)?.count>0){
                itemRemoveArray = itemRemoveDict.valueForKey(itemObj.is_item_code) as! NSMutableArray;
                itemRemoveArray.removeLastObject();
            }
        }
        if(itemRemoveArray.count>0){
            itemRemoveDict .setObject(itemRemoveArray, forKey: itemObj.is_item_code);
        }
        else{
            itemRemoveDict .removeObjectForKey(itemObj.is_item_code);
        }
        
        if(itemRemoveDict.count>0){
            Helper.saveObjectDict(SELECTED_ITEM_KEY, objArray: itemRemoveDict);
        }
        else{
            Helper.removeUserDefault(SELECTED_ITEM_KEY);
        }
        
        
        itemCount(itemRemoveArray.count);
        
        
        NSNotificationCenter.defaultCenter().postNotificationName("SELECTED_ITEM_NOTIFY", object: nil);
        //self.delegate?.tableReload();
    }
    
    
    
    func itemCount(itemValue:Int){
        self.lblItemNo.text = "\(itemValue)";
        
    }
    
}
