//
//  OrderBillCell.swift
//  LazyLad
//
//  Created by Lazylad on 6/18/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class OrderBillCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblItemName: UILabel!

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindOrderBillDetailsObject(billObject:NZBillOrderItems){
        self.lblItemName.text = billObject.order_item_name;
        self.lblPrice.text  = "₹ \( billObject.ordr_item_cost)";
        self.lblDetails.text = billObject.ordr_item_description;
        self.lblTotalPrice.text = "₹ \( billObject.ordr_item_cost)";
    }
    
}
