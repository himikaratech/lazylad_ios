//
//  ShopViewCell.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//
//₹
import UIKit

class ShopViewCell: UITableViewCell {

    @IBOutlet weak var lblstrike: UILabel!
    @IBOutlet var imgItem: UIImageView!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var lblItemType: UILabel!
    @IBOutlet var lblItemMRPRate: UILabel!
    @IBOutlet var lblItemFinalRate: UILabel!
    @IBOutlet var btnIteamCountIncrease: UIButton!
    @IBOutlet var btnItemCountDecrease: UIButton!
    @IBOutlet var lblItemCount: UILabel!
    var itemObj:NZItemsServiceProviderCategoryWise!;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    func bindDataWithitemsServiceProvider(itemObject:NZItemsServiceProviderCategoryWise){
        itemObj = itemObject;
        self.lblItemName.text = itemObject.is_item_name;
        self.lblItemType.text = itemObject.is_item_desc;
        if(itemObject.is_item_mrp > itemObject.is_item_cost){
        self.lblItemMRPRate.text = "₹ \(toString(itemObject.is_item_mrp))";
            self.lblstrike.hidden = false;
            self.lblItemMRPRate.hidden = false;
        }
        else{
             self.lblstrike.hidden = true;
            self.lblItemMRPRate.hidden = true;
        }
        self.lblItemFinalRate.text = "₹ \(toString(itemObject.is_item_cost))";
        self.imgItem.sd_setImageWithURL(NSURL(string: itemObject.is_item_img_address), placeholderImage: UIImage(named: ""));
        var itemDict = NSMutableDictionary();
        var itemArray = NSMutableArray();
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            itemDict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
            if(itemDict.valueForKey(itemObj.is_item_code)?.count>0){
                itemArray = itemDict.valueForKey(itemObj.is_item_code) as! NSMutableArray;
                itemCount(itemArray.count);
            }
            
        }


    }
    
    
    @IBAction func iteamCountIncreaceAction(sender: AnyObject) {
        
        //Save Items Array into Dict
        var itemDict = NSMutableDictionary();
        var itemAddArray = NSMutableArray();
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            itemDict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
            if(itemDict.valueForKey(itemObj.is_item_code)?.count>0){
              itemAddArray = itemDict.valueForKey(itemObj.is_item_code) as! NSMutableArray;
            }
            
        }
        itemAddArray.addObject(itemObj);
        itemDict .setObject(itemAddArray, forKey: itemObj.is_item_code);
        Helper.saveObjectDict(SELECTED_ITEM_KEY, objArray: itemDict);
        itemCount(itemAddArray.count);
        
        
        
        //Save Selected Items
//        var itemAddArray = NSMutableArray();
//        if(Helper.fetchObjectArray(itemObj.is_item_code).count>0){
//            itemAddArray = Helper.fetchObjectArray(itemObj.is_item_code);
//        }
//        itemAddArray.addObject(itemObj);
//        Helper.saveObjectArray(itemObj.is_item_code, objArray: itemAddArray);
//        itemCount(itemAddArray.count);
        NSNotificationCenter.defaultCenter().postNotificationName("SELECTED_ITEM_NOTIFY", object: nil);
    }
   
    
    @IBAction func iteamCountDecreaseAction(sender: AnyObject) {
        
        
        // Remove Item From Dict
        var itemRemoveDict = NSMutableDictionary();
        var itemRemoveArray = NSMutableArray();
        if(Helper.fetchObjectDict(SELECTED_ITEM_KEY).count>0){
            itemRemoveDict = Helper.fetchObjectDict(SELECTED_ITEM_KEY);
            if(itemRemoveDict.valueForKey(itemObj.is_item_code)?.count>0){
            itemRemoveArray = itemRemoveDict.valueForKey(itemObj.is_item_code) as! NSMutableArray;
            itemRemoveArray.removeLastObject();
            }
        }
       
        //itemRemoveDict .setObject(itemRemoveArray, forKey: itemObj.is_item_code);
       // Helper.saveObjectDict(SELECTED_ITEM_KEY, objArray: itemRemoveDict);
        
        if(itemRemoveArray.count>0){
            itemRemoveDict .setObject(itemRemoveArray, forKey: itemObj.is_item_code);
        }
        else{
            itemRemoveDict .removeObjectForKey(itemObj.is_item_code);
        }
        
        if(itemRemoveDict.count>0){
            Helper.saveObjectDict(SELECTED_ITEM_KEY, objArray: itemRemoveDict);
        }
        else{
            Helper.removeUserDefault(SELECTED_ITEM_KEY);
        }
        
        itemCount(itemRemoveArray.count);
        
        
        
        // Remove Items
//        var itemRemoveArray = NSMutableArray();
//        if(Helper.fetchObjectArray(itemObj.is_item_code).count>0){
//            itemRemoveArray = Helper.fetchObjectArray(itemObj.is_item_code);
//            itemRemoveArray.removeLastObject();
//        }
//        Helper.saveObjectArray(itemObj.is_item_code, objArray:itemRemoveArray);
//        itemCount(itemRemoveArray.count);
         NSNotificationCenter.defaultCenter().postNotificationName("SELECTED_ITEM_NOTIFY", object: nil);
    }
    
    
    
    
    func itemCount(itemValue:Int){
        self.lblItemCount.text = "\(itemValue)";
        
    }
}