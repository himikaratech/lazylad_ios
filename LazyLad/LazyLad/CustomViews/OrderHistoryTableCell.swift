//
//  OrderHistoryTableCell.swift
//  LazyLad
//
//  Created by Lazylad on 6/17/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class OrderHistoryTableCell: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblQuantity: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var imgItems: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindWithOrderDetailsObj(previousDetails:NZPreviousOrderDetails){
        self.lblName.text = previousDetails.order_item_name;
        self.lblPrice.text = "₹ \(previousDetails.ordr_item_cost)";
        self.lblQuantity.text = previousDetails.order_item_quantity;
        self.lblType.text = previousDetails.ordr_item_description;
        self.imgItems.sd_setImageWithURL(NSURL(string: previousDetails.order_smg_address), placeholderImage: UIImage(named: ""));
    }
    
}
