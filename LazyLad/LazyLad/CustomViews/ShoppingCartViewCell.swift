//
//  ShoppingCartViewCell.swift
//  LazyLad
//
//  Created by Lazylad on 6/12/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class ShoppingCartViewCell: UITableViewCell {

    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var lblRate: UILabel!
    @IBOutlet var lblTotalItem: UILabel!
    @IBOutlet var imgItemImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bindDataObject(itemObject:NZItemsServiceProviderCategoryWise , itemNo:Int){
        self.lblItemName.text = itemObject.is_item_name;
        self.lblRate.text = itemObject.is_item_desc;
        self.lblTotalItem.text = "₹ \(itemObject.is_item_mrp)x\(itemNo)"
        self.imgItemImage.sd_setImageWithURL(NSURL(string: itemObject.is_item_img_address), placeholderImage: UIImage(named: ""));
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
