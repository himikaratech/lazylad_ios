//
//  StoreViewCell.swift
//  LazyLad
//
//  Created by Lazylad on 6/4/15.
//  Copyright (c) 2015 Lazylad. All rights reserved.
//

import UIKit

class StoreViewCell: UITableViewCell {

    @IBOutlet var lblStoreName: UILabel!
    @IBOutlet var lblStoreTimings: UILabel!
    @IBOutlet var lblStoreDilivaryRate: UILabel!
   
    @IBOutlet var lblTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindServiceObject(serviceObject:NZServiceProvider)->AnyObject{
        lblStoreName.text = serviceObject.sp_name;
        lblStoreTimings.text = "Store Timings 9AM - 9PM";
        lblStoreDilivaryRate.text = "Free delivery above ₹ \(serviceObject.sp_min_order)" ;
        lblTime.text = serviceObject.sp_del_time;
        return self;
    }
    
}
